This is the second iteration of [Assembly Archive](https://archive.assembly.org/).

## Pre-requisites

* Likely a modern GNU/Linux system to handle all the build time bash
  magic. Windows Subsystem for Linux also works.
* [Bazel](https://bazel.build/). Preferably by using
  [bazelisk](https://github.com/bazelbuild/bazelisk) to handle Bazel versioning.
* C compiler to compile golang runtime.
* Java runtime environment to run [YUI
  compressor](https://yui.github.io/yuicompressor/).
* [zopflipng](https://github.com/google/zopfli) for image compression.

Recommended requirement:

* [iBazel](https://github.com/bazelbuild/bazel-watcher) to get
  immediate updates into use when source code changes.

## Building

To create a build for distribution, you can use `bazel build ...:all`
command to build everything. This will then create a distribution
tarball at `bazel-bin/assembly-archive.tar`. This also supports
cross compilation against different platforms, so you can create
binaries that work on different systems without actually having to
compile on such systems:

```bash
# Compile for the current host platform:
$ bazel build //:assembly-archive-pkg
# Compile for Raspberry Pi:
$ bazel build //:assembly-archive-pkg --platforms=@io_bazel_rules_go//go/toolchain:linux_arm
# Compile for macOS:
$ bazel build //:assembly-archive-pkg --platforms=@io_bazel_rules_go//go/toolchain:darwin_amd64
# Compile for Linux:
$ bazel build //:assembly-archive-pkg --platforms=@io_bazel_rules_go//go/toolchain:linux_amd64
# Compile for Windows:
$ bazel build //:assembly-archive-pkg --platforms=@io_bazel_rules_go//go/toolchain:windows_amd64
```

### Creating containers

You may create a container image if you prefer to execute containers
instead. These require you to specify correct target platform for the
system you want to run:

```bash
$ bazel build //container \
    --platforms=@io_bazel_rules_go//go/toolchain:linux_amd64 \
    --@io_bazel_rules_go//go/config:static
```

This then makes this container image available as
`bazel-bin/container/assembly-archive.tar` that can be imported into a
local Docker instance with `docker load` command:

```bash
$ docker load --input bazel-bin/container/assembly-archive.tar
Loaded image: registry.gitlab.com/barro/assembly-archive/assembly-archive:latest
```

### Creating SystemD unit

SystemD unit file for Assembly Archive can be created by building
`//service:assembly-archive-service` target with appropriate defines:

```bash
bazel build //service:assembly-archive-service \
    --define ASMARCHIVE_USER=asmarchive \
    --define ASMARCHIVE_GROUP=asmarchive \
    --define ASMARCHIVE_ENVIRONMENT_FILE=/assembly-archive/assembly-archive.env \
    --define ASMARCHIVE_BIN_DIR=/assembly-archive/bin
```

This will then create `bazel-bin/service/assembly-archive-service.tar`
that holds following SystemD units and a launcher binary that should
be extracted under `/usr/local`:

* `assembly-archive.service` launches the server executable.
* `assembly-archive-watcher.service` re-launches the service. Used for
  service updates.
* `assembly-archive-watcher.path` monitors paths under
  `ASMARCHIVE_BIN_DIR` for updates and restarts the service when such
  things is detected.

## Running

Assembly archive expects a reverse proxy that only exposes the
`/site/` namespace to the public when in production mode as the root
path.

The extracted distribution package provides `assembly-archive`
executable that includes all the application logic. This listens to
`localhost` at port `8080` and you may want to change that for
production:

```bash
$ ./assembly-archive -host 0.0.0.0 -port 1234
2019/07/20 12:52:54 Listening to 0.0.0.0:1234
```

Also to enable `/api/` usage, you need to create a plain text file
that defines the API credentials for updates. By default this reads
`auth.txt` but it can be configured with `-authfile` parameter. The
format of this file is to have `USERNAME:PASSWORD` combination on each
line. This format intentionally does not hash passwords. Here are
example commands to create `/api/` namespace access::

```bash
$ touch auth.txt
$ chmod 600 auth.txt
$ echo username:password > auth.txt
$ ./assembly-archive -authfile auth.txt
```

### Configuration

Assembly archive can be configured either by command line switches or
by using environment variables. Following options are available:

<table>
<tr>
	<th>Command line flag</th>
	<th>Environment variable</th>
	<th>Default</th>
	<th>Allowed values</th>
	<th>Description</th>
</tr>
<tr>
	<td><code>-authfile &lt;value></code></td>
	<td><code>ASMA_AUTHFILE=&lt;value></code></td>
	<td><code>auth.txt</code></td>
	<td>Valid file path</td>
	<td>
		File with plain text <code>username:password</code> lines. This
		authentication information is used to authenticate against the
		API. File needs to be readable only by the file owner.
	</td>
</tr>
<tr>
	<td><code>-dev</code></td>
	<td><code>ASMA_DEV=0/1</code></td>
	<td><code>0</code></td>
	<td>0 or 1 for the environment variable.</td>
	<td>
		Enable development mode. Development mode enables using
		<code>/exit</code> path to shut down Assembly archive
		application and prepends <code>-root-site</code> option's
		value to every link.
	</td>
</tr>
<tr>
	<td><code>-dir-data &lt;value></code></td>
	<td><code>ASMA_DIR_DATA=&lt;value></code></td>
	<td><code>_data</code></td>
	<td>An existing directory.</td>
	<td>
		Data directory is a place where all pictures and metadata
		files that are stored on repository update. This directory is
		exposed by Assembly archive at
		<code>&lt;site-root>/_data/</code> path to the public as is.
	</td>
</tr>
<tr>
	<td><code>-dir-static &lt;value></code></td>
	<td><code>ASMA_DIR_STATIC=&lt;value></code></td>
	<td><code>static</code></td>
	<td>Directory with expected static assets.</td>
	<td>
		Directory for files that form the static assets (graphics,
		stylesheets) of Assembly Archive. This is supplied next to the
		binary and usually you only need to override this if you are
		modifying static assets without the build system support.
	</td>
</tr>
<tr>
	<td><code>-dir-templates &lt;value></code></td>
	<td><code>ASMA_DIR_TEMPLATES=&lt;value></code></td>
	<td><code>templates</code></td>
	<td>Directory with expected layout templates.</td>
	<td>
		Directory for files hold the dynamic page layout of Assembly
		Archive. This is supplied next to the binary and usually you
		only need to override this if you are doing quick layout
		template change iteration without build system support.
	</td>
</tr>
<tr>
	<td><code>-host &lt;value></code></td>
	<td><code>ASMA_HOST=&lt;value></code></td>
	<td><code>localhost</code></td>
	<td>Hostname or an IP address.</td>
	<td>
		Interface hostname or IP address where Assembly archive
		listens for connections. Use <code>0.0.0.0</code> to listen to
		all interfaces.
	</td>
</tr>
<tr>
	<td><code>-port &lt;value></code></td>
	<td><code>ASMA_PORT=&lt;value></code></td>
	<td><code>8080</code></td>
	<td>Valid TCP port number (1-65535).</td>
	<td>
		TCP port number that Assembly archive listens for
		connections.
	</td>
</tr>
<tr>
	<td><code>-root-api &lt;value></code></td>
	<td><code>ASMA_ROOT_API=&lt;value></code></td>
	<td><code>/api</code></td>
	<td>Valid URL path.</td>
	<td>
		Path where the internal API to update this site is found.
	</td>
</tr>
<tr>
	<td><code>-root-site &lt;value></code></td>
	<td><code>ASMA_ROOT_SITE=&lt;value></code></td>
	<td><code>/site</code></td>
	<td>Valid URL path.</td>
	<td>
		Path where the publicly accessible site resides in. This
		should be accessed by a reverse proxy that uses this path as a
		base for requests and serves resources without any prefixes.
	</td>
</tr>
</table>

## Development

In development you can run locally in `-dev` mode. This basically
entails the standard hack, build, test cycle where the build and test
phases can be done by using Bazel. It is recommended to use
[iBazel](https://github.com/bazelbuild/bazel-watcher) to get make
rebuild, restart, and retest operations done automatically:

Run the development server:

```bash
# You can run the latest version with Bazel. You need to manually
# press ctrl-c to stop the process and start it again:
$ bazel run //:devserver -- -dev
# ibazel command does restarts for you whenever something changes:
$ ibazel run //:devserver -- -dev
```

Run tests:

```bash
$ bazel test ...:all
# ibazel will re-run tests immediately when something changes:
$ ibazel test ...:all
```

### Updating application dependencies

Update [rules_go](https://github.com/bazelbuild/rules_go/) by checking
[github.com/bazelbuild/rules_go/releases](https://github.com/bazelbuild/rules_go/releases)
and updating the [`WORKSPACE.bazel`](WORKSPACE.bazel) file.

Update [gazelle](https://github.com/bazelbuild/bazel-gazelle) by
checking
[github.com/bazelbuild/rules_docker/releases](https://github.com/bazelbuild/rules_docker/releases)
and updating the [`WORKSPACE.bazel`](WORKSPACE.bazel) file.

Update [rules_docker](https://github.com/bazelbuild/rules_docker) by
checking
[github.com/bazelbuild/bazel-gazelle](https://github.com/bazelbuild/bazel-gazelle)
and updating the [`WORKSPACE.bazel`](WORKSPACE.bazel) file.

Update extended go library dependencies with Gazelle:

```bash
bazel run :gazelle -- update-repos golang.org/x/xerrors golang.org/x/net golang.org/x/text
```

### Updating Gitlab CI dependencies

As Assembly archive uses Bazel to build itself and all the
dependencies, it needs an operating system image that supports
this. The base image is indicated in
[`.gitlab-ci.yml`](.gitlab-ci.yml), and is currently
[Ubuntu 20.04](https://releases.ubuntu.com/20.04/). Other dependencies
are found from [`ci/ubuntu-setup.sh`](ci/ubuntu-setup.sh) file
and the main dependency that needs to be kept up to date over time is
[Bazelisk](https://github.com/bazelbuild/bazelisk).

Newest releases for Bazel and Bazelisk can be found from:

* [Bazel releases](https://github.com/bazelbuild/bazel/releases) in
  [`.bazeliskrc`](.bazeliskrc) file.
* [Bazelisk releases](https://github.com/bazelbuild/bazelisk/releases)
  in [`ci/gitlab-ci-setup.sh`](ci/gitlab-ci-setup.sh) file.

## Deploying

The
[GitLab instance of this repository](https://gitlab.com/Barro/assembly-archive)
provides pre-built binaries that have a relatively short
lifetime. When a newer version of Assembly Archive binaries is
published, it can be verified with following type of command:

```bash
openssl dgst \
    -verify assembly-archive.pub.pem \
    -signature assembly-archive-linux_amd64.tar.sig \
    assembly-archive-linux_amd64.tar
```

The contents of `assembly-archive.pub.pem` file currently is:

```
-----BEGIN PUBLIC KEY-----
MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEOd3EAKmlJPzppm3WTSlr6RjhSHrxishA
PGXuelBjmnIOOKOtL+REtkc1o4btrvVvgtHY+xV75hqi+CEYAQoBsw==
-----END PUBLIC KEY-----
```

This signature verification and replacing old binaries with a newer
ones is done automatically with
[`ci/update-binaries.sh`](ci/update-binaries.sh) script.
