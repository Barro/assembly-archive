#!/usr/bin/env python3

import ast
import argparse
import dataclasses
import hashlib
import json
import logging
import os
import os.path
import random
import re
import subprocess
import sys
import time
import urllib.error
import urllib.request


@dataclasses.dataclass
class GithubReleaseArtifact:
    project: str
    tag: str
    tag_stripped: str = None
    name: str = None
    url: str = None
    sha256: str = None


def download_url(url):
    data = None
    for _ in range(5):
        try:
            request = urllib.request.urlopen(url)
            data = request.read()
            break
        except urllib.error.URLError:
            logging.warning("Failed to download %s, retrying.", url)
        time.sleep(random.random() * 30.0)
    return data


def latest_github_release(*, project, regex_template):
    data_bytes = download_url("https://api.github.com/repos/%s/releases/latest" % project)
    data = json.loads(data_bytes.decode("utf-8"))
    artifact = GithubReleaseArtifact(project=project, tag=data["tag_name"])
    artifact.tag_stripped = artifact.tag.lstrip("v")
    matcher_regex = re.compile(regex_template % {"tag": artifact.tag})
    for asset in data["assets"]:
        artifact.name = asset["name"]
        if matcher_regex.match(artifact.name):
            artifact.url = asset["browser_download_url"]
            asset_data = download_url(asset["browser_download_url"])
            artifact.sha256 = hashlib.sha256(asset_data).hexdigest()
            break
    if artifact.sha256 is None:
        return None
    return artifact


def update_bazel_workspace(workspace_data, release):
    input_lines = workspace_data.splitlines()
    output_lines = []
    skip_next = False
    matcher_sha256 = re.compile("( +)# deps-update:%s sha256" % release.project)
    matcher_url = re.compile("( +)# deps-update:%s url (.+)" % release.project)
    matcher_strip_prefix = re.compile("( +)# deps-update:%s strip_prefix (.+)" % release.project)
    for line in input_lines:
        if skip_next:
            skip_next = False
            continue
        match_sha256 = matcher_sha256.match(line)
        if match_sha256:
            output_lines.append(line)
            spaces = match_sha256.group(1)
            output_lines.append(
                "%ssha256 = \"%s\"," % (spaces, release.sha256))
            skip_next = True
            continue
        match_url = matcher_url.match(line)
        if match_url:
            output_lines.append(line)
            spaces = match_url.group(1)
            template = match_url.group(2)
            url = template % {
                "tag": release.tag,
                "tag-stripped": release.tag_stripped,
                "name": release.name,
                "url": release.url,
                }
                
            output_lines.append("%s\"%s\"," % (spaces, url))
            skip_next = True
            continue
        match_strip_prefix = matcher_strip_prefix.match(line)
        if match_strip_prefix:
            output_lines.append(line)
            spaces = match_strip_prefix.group(1)
            template = match_strip_prefix.group(2)
            strip_prefix = template % {
                "tag": release.tag,
                "tag-stripped": release.tag_stripped,
                "name": release.name,
                "url": release.url,
                }
                
            output_lines.append("%sstrip_prefix = \"%s\"," % (spaces, strip_prefix))
            skip_next = True
            continue
        output_lines.append(line)
    return "\n".join(output_lines) + "\n"


def update_bazel_workspace_dependencies(releases, workspace_file):
    with open(workspace_file) as fp:
        workspace_data = fp.read()

    for release in releases.values():
        workspace_data = update_bazel_workspace(workspace_data, release)

    with open(workspace_file, "w") as fp:
        fp.write(workspace_data)


def update_bazeliskrc(releases, bazeliskrc_file):
    bazeliskrc_data = "USE_BAZEL_VERSION=%(tag-stripped)s\n" % {
        "tag-stripped": releases["bazel"].tag_stripped}
    with open(bazeliskrc_file, "w") as fp:
        fp.write(bazeliskrc_data)


def update_gazelle_deps(repo_root):
    subprocess.run(
        ["bazel", "run", ":gazelle", "--", "update-repos", "golang.org/x/xerrors", "golang.org/x/net", "golang.org/x/text"],
        cwd=repo_root,
        check=True)


def main(argv):
    parser = argparse.ArgumentParser(prog=argv[0])
    parser.add_argument("--repo-root", required=True)
    args = parser.parse_args(argv[1:])
    releases = {
        "bazel-gazelle": latest_github_release(
            project = "bazelbuild/bazel-gazelle",
            regex_template = "bazel-gazelle-%(tag)s\\.",
        ),
        "bazelisk": latest_github_release(
            project = "bazelbuild/bazelisk",
            regex_template = "^bazelisk-linux-amd64$",
        ),
        "rules_docker": latest_github_release(
            project = "bazelbuild/rules_docker",
            regex_template = "rules_docker-%(tag)s\\.",
        ),
        "rules_go": latest_github_release(
            project = "bazelbuild/rules_go",
            regex_template = "rules_go-%(tag)s\\.",
        ),
        "bazel": latest_github_release(
            project = "bazelbuild/bazel",
            regex_template = ".+\\.sha256$",
        ),
    }

    workspace_file = os.path.join(args.repo_root, "WORKSPACE.bazel")
    update_bazel_workspace_dependencies(releases, workspace_file)

    bazeliskrc_file = os.path.join(args.repo_root, ".bazeliskrc")
    update_bazeliskrc(releases, bazeliskrc_file)

    update_gazelle_deps(args.repo_root)

    return os.EX_OK


if __name__ == "__main__":
    sys.exit(main(sys.argv))
