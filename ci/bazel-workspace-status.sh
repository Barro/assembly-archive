#!/usr/bin/env bash

set -euo pipefail

version=$(git show --no-patch --date=format:%Y-%m-%d --format=%cd+%h)
echo "STABLE_ASM_ARCHIVE_VERSION $version"
