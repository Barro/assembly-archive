#!/bin/sh

set -x
set -u
set -e

docker build -t registry.gitlab.com/barro/assembly-archive/builder:latest .
docker push registry.gitlab.com/barro/assembly-archive/builder:latest
