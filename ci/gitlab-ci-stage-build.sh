#!/usr/bin/env bash

set -xeuo pipefail

ci/prepare-cache.sh "$GIT_CLONE_PATH/.cache" "$CACHES_ROOT/save"

# These signing keys have been created with following commands:
#
# openssl genpkey -algorithm ec -pkeyopt ec_paramgen_curve:secp256k1 -pkeyopt ec_param_enc:named_curve -out assembly-archive-gitlab-releases.priv.pem
# openssl pkey -in assembly-archive-gitlab-releases.priv.pem -pubout -out assembly-archive-gitlab-releases.pub.pem
XDG_CACHE_HOME=$CACHES_ROOT/save \
    ci/gitlab-ci-build.sh "${GITLAB_RELEASES_KEY_PRIVATE_FILE-:/dev/no-release-key-file-given}"
cp "$GITLAB_RELEASES_KEY_PUBLIC_FILE" assembly-archive.pub.pem

ci/cleanup-cache.sh "$CACHES_ROOT/save"
ci/prepare-cache.sh "$CACHES_ROOT/save" "$GIT_CLONE_PATH/.cache"
