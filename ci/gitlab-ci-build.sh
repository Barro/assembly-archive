#!/usr/bin/env bash

set -xeuo pipefail

CACHES_ROOT=${CACHES_ROOT:-$USER/.cache/}

{
    echo "startup --output_user_root=$CACHES_ROOT/save/bazel"
    echo "startup --output_base=$CACHES_ROOT/bazel"
} > ~/.bazelrc

# Reveal the used Bazel version for debugging.
bazel version

GITLAB_RELEASES_KEY_PRIVATE_FILE=${1:-/dev/no-release-key-file-given}

# First do a sanity check by running tests on the host platform.
bazel test ...:all

# Enable more platforms if the need arises to distribute them::
TARGET_PLATFORMS=(
    # darwin_amd64
    linux_amd64
    # linux_arm
    # windows_amd64
)
# Create releases for every target platform that we want to
# support. There should be just 1, as there is just 1 production
# instance. But we could have more binaries to get started with more
# easy user interface development.
for platform in "${TARGET_PLATFORMS[@]}"; do
    bazel build ...:all \
          --stamp \
          --workspace_status_command=ci/bazel-workspace-status.sh \
          --platforms=@io_bazel_rules_go//go/toolchain:"$platform" \
	  --@io_bazel_rules_go//go/config:static
    cp bazel-bin/assembly-archive-pkg.tar assembly-archive-"$platform".tar

    if [[ -f $GITLAB_RELEASES_KEY_PRIVATE_FILE ]]; then
        bazel run \
              --stamp \
              --workspace_status_command=ci/bazel-workspace-status.sh \
              --platforms=@io_bazel_rules_go//go/toolchain:"$platform" \
              :sign-pkg -- \
              "$GITLAB_RELEASES_KEY_PRIVATE_FILE"
        cp bazel-bin/assembly-archive-pkg.tar.sig assembly-archive-"$platform".tar.sig
    else
        echo "NO-SIGNATURE-IN-UNPROTECTED-BRANCH" > assembly-archive-"$platform".tar.sig
    fi
done
