#!/usr/bin/env bash

set -euo pipefail

CACHE_DIR=$1

chmod -R u+rw "$CACHE_DIR"
find "$CACHE_DIR" -type f -mtime +30 -delete
find "$CACHE_DIR" -type d -mtime +30 -delete 2>/dev/null || :
