#!/usr/bin/env bash

set -xeuo pipefail

# See https://github.com/bazelbuild/bazelisk/releases for the newest revision.
BAZELISK_VERSION=v1.11.0
BAZELISK_SHA256=231ec5ca8115e94c75a1f4fbada1a062b48822ca04f21f26e4cb1cd8973cd458
BAZELISK_URL=https://github.com/bazelbuild/bazelisk/releases/download/$BAZELISK_VERSION/bazelisk-linux-amd64

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get upgrade -y
apt-get install -y \
    curl \
    gcc \
    git \
    openjdk-11-jre-headless \
    python3 \
    zopfli
# Only provide Python 3. No Python 2 on the system.
update-alternatives --install /usr/bin/python python /usr/bin/python3 1

curl --location --retry-connrefused --retry 16 -o /usr/local/bin/bazel "$BAZELISK_URL"
checksum=$(sha256sum -b /usr/local/bin/bazel | cut -f 1 -d " ")
if [[ $checksum != $BAZELISK_SHA256 ]]; then
    echo >&2 "Calculated checksum '$checksum' of Bazelisk from '$BAZELISK_URL' did not match the expected '$BAZELISK_SHA256'"
    exit 1
fi
chmod 755 /usr/local/bin/bazel
# Make Bazelisk to download the set Bazel version:
(
    set -euo pipefail
    cd /data/
    touch WORKSPACE
    bazel clean --expunge
    rm WORKSPACE
)
