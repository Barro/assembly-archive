#!/usr/bin/env bash

set -xeuo pipefail

FROM=$1
TO=$2

mkdir -p "$FROM" "$TO"
rmdir "$TO"
mv "$FROM" "$TO"
