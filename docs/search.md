# Search operators

All search queries are case insensitive. By default each space
separates a keyword that is required to appear in the search string.

Following search operators are supported:

* `"keywords with spaces"` matches the keyword string including spaces.
* `a:artist` matches artist's name.
* `t:title` matches entry title.
* `s:section` matches all entries in a section.
* `y:year` matches all entries for a year.

Operators can be combined: `y:2010 a:artist` would match all entries
during year 2010 by artist.

## JSON API

Search also supports returning its results in JSON format. This can be
achieved by adding `&fmt=json` parameter to the search page. The
amount of results returned at once can be limited by `&limit=10`
parameter.
