package testsite

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"asma/base"
	"asma/search"
	"asma/server"
	"asma/site"
	"asma/state"
)

type TestSiteContext struct {
	T        *testing.T
	Settings base.SiteSettings
	State    *state.SiteState
	Search   chan search.SearchQuery
	Renderer http.HandlerFunc
}

func create_settings() base.SiteSettings {
	return base.SiteSettings{
		SiteRoot:     "",
		DataDir:      "_data",
		StaticDir:    "test/static/static",
		TemplatesDir: "test/templates/templates",
	}
}

func create_entry(path_prefix string, id int) base.Entry {
	entry_title := fmt.Sprintf("Entry %d", id)
	entry_author := fmt.Sprintf("Author %d", id)
	entry_key := fmt.Sprintf("entry-%d-by-author-%d", id, id)
	entry_path := fmt.Sprintf("%s/%s", path_prefix, entry_key)

	asset := base.Asset{
		Type: "image",
		Data: state.ImageAsset{},
	}
	return base.Entry{
		Path:   entry_path,
		Title:  entry_title,
		Key:    entry_key,
		Author: entry_author,
		Asset:  asset,
	}
}

func create_state() state.SiteState {
	state := state.SiteState{SiteRoot: "", DataDir: "_data"}
	years := []*base.Year{}
	for i := 2000; i < 2030; i++ {
		year_key := strconv.Itoa(i)
		year_path := year_key
		year := base.Year{
			Year: i,
			Key:  year_key,
			Path: year_path,
		}
		for j := 0; j < 15; j++ {
			section_name := fmt.Sprintf("Section %d", j)
			section_key := fmt.Sprintf("section-%d", j)
			section_path := fmt.Sprintf("%s/%s", year_path, section_key)
			section := base.Section{
				Path:        section_path,
				Name:        section_name,
				Key:         section_key,
				Description: "Section description",
			}
			for k := 0; k < 1.5*site.MAX_SECTION_DISPLAY_ENTRIES; k++ {
				entry := create_entry(section_path, k)
				section.Entries = append(section.Entries, &entry)
			}
			year.Sections = append(year.Sections, &section)
		}
		years = append(years, &year)
	}
	state.Years = years
	return state
}

func create_search() chan search.SearchQuery {
	channel := make(chan search.SearchQuery, 1)
	return channel
}

func CreateContext(t *testing.T) TestSiteContext {
	settings := create_settings()
	state := create_state()
	search := create_search()
	renderer := site.SiteRenderer(settings, &state, search)
	return TestSiteContext{
		T:        t,
		Settings: settings,
		State:    &state,
		Search:   search,
		Renderer: renderer,
	}
}

func DoVerifyRequest(
	context TestSiteContext,
	path string,
	wanted_code int) *httptest.ResponseRecorder {
	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		context.T.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(server.StripPrefix("/", context.Renderer))
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != wanted_code {
		context.T.Errorf(
			"Unexpected status code for path '%s'. Got %v want %v",
			path,
			status,
			wanted_code)
	}
	return rr
}
