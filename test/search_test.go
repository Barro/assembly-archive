package search_test

import (
	"sort"
	"strings"
	"testing"

	"asma/search"
)

func TestTermsNormalizationShouldNormalizeFullWidthText(t *testing.T) {
	normalized := search.NormalizeTerms("ａｂｃ")
	wanted := "abc"
	if normalized != wanted {
		t.Errorf("Normalized %#U != %#U", normalized, wanted)
	}
}

func TestTermsNormalizationShouldNormalizeAccentsOut(t *testing.T) {
	normalized := search.NormalizeTerms("äb́ḉ")
	wanted := "abc"
	if normalized != wanted {
		t.Errorf("Normalized %#U != %#U", normalized, wanted)
	}
}

func TestTermsNormalizationShouldNormalizeWidths(t *testing.T) {
	normalized := search.NormalizeTerms("ａｂｃ ｶﾅ")
	wanted := "abc カナ"
	if normalized != wanted {
		t.Errorf("Normalized %#U != %#U", normalized, wanted)
	}
}

func TestTermsNormalizationShouldNormalizeCase(t *testing.T) {
	normalized := search.NormalizeTerms("ABC ÄÖ")
	wanted := "abc ao"
	if normalized != wanted {
		t.Errorf("Normalized %#U != %#U", normalized, wanted)
	}
}

func str_keyword_arrays_equal(first []string, second []string) bool {
	if len(first) != len(second) {
		return false
	}
	sort.Strings(first)
	sort.Strings(second)
	for i := 0; i < len(first); i++ {
		if first[i] != second[i] {
			return false
		}
	}
	return true
}

func check_keywords(t *testing.T, normalized []string, wanted []string) {
	if !str_keyword_arrays_equal(normalized, wanted) {
		t.Errorf(
			"Keywords were not properly handled %s != %s",
			normalized,
			wanted)
	}
}

func TestEmptyKeywordsShouldReturnEmptyArray(t *testing.T) {
	normalized_empty := search.SplitKeywords("")
	check_keywords(t, normalized_empty, []string{})
	normalized_spaces := search.SplitKeywords("  ")
	check_keywords(t, normalized_spaces, []string{})
}

func TestKeywordsShouldBeSplitBySpaces(t *testing.T) {
	normalized_one := search.SplitKeywords("a b c")
	wanted_one := []string{"a", "b", "c"}
	check_keywords(t, normalized_one, wanted_one)
	normalized_trim := search.SplitKeywords(" a  b c ")
	wanted_trim := []string{"a", "b", "c"}
	check_keywords(t, normalized_trim, wanted_trim)
}

func TestQuotedKeywordsShouldBeMaintained(t *testing.T) {
	normalized_beg := search.SplitKeywords("\"a b\" c")
	wanted_beg := []string{"a b", "c"}
	check_keywords(t, normalized_beg, wanted_beg)

	normalized_mid := search.SplitKeywords("a \"b\" c")
	wanted_mid := []string{"a", "b", "c"}
	check_keywords(t, normalized_mid, wanted_mid)

	normalized_end := search.SplitKeywords("a \"b c\"")
	wanted_end := []string{"a", "b c"}
	check_keywords(t, normalized_end, wanted_end)
}

func TestDuplicateKeywordsShouldBeRemoved(t *testing.T) {
	normalized := search.SplitKeywords("a a")
	wanted := []string{"a"}
	check_keywords(t, normalized, wanted)
}

func TestLongSearchStringShouldBeCut(t *testing.T) {
	original := strings.Repeat("a", 1024)
	normalized := search.NormalizeTerms(original)
	if len(normalized) >= len(original) {
		t.Errorf(
			"Search should not be original big string of %d characters",
			len(normalized))
	}
}

func TestSearchOperatorsShouldBeUpperCased(t *testing.T) {
	converted := search.ConvertSearchOperators(
		[]string{"a:a", "t:a", "y:1", "s:a"})
	wanted := []string{"A:a|", "T:a|", "Y:1|", "S:a|"}
	check_keywords(t, converted, wanted)
}
