package site_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"testing"

	"asma/search"
	"asma/site"
	"asma/testsite"
)

func TestMainPageShouldRenderWithoutErrors(t *testing.T) {
	context := testsite.CreateContext(t)
	testsite.DoVerifyRequest(context, "/", http.StatusOK)
}

func TestYearPageShouldRenderWithoutErrors(t *testing.T) {
	context := testsite.CreateContext(t)
	testsite.DoVerifyRequest(context, "/2000", http.StatusOK)
}

func TestSectionPageShouldRenderWithoutErrors(t *testing.T) {
	context := testsite.CreateContext(t)
	testsite.DoVerifyRequest(context, "/2000/section-1", http.StatusOK)
}

func TestEntryPageShouldRenderWithoutErrors(t *testing.T) {
	context := testsite.CreateContext(t)
	testsite.DoVerifyRequest(
		context, "/2000/section-1/entry-1-by-author-1", http.StatusOK)
}

func TestSearchPageWithoutQueryShouldRenderWithoutErrors(t *testing.T) {
	context := testsite.CreateContext(t)
	testsite.DoVerifyRequest(context, "/search", http.StatusOK)
}

func TestSearchPageWithEmptyQueryShouldRenderWithoutErrors(t *testing.T) {
	context := testsite.CreateContext(t)
	testsite.DoVerifyRequest(context, "/search?q=", http.StatusOK)
}

func TestSearchPageWithEmptyQueryResultShouldRenderWithoutErrors(t *testing.T) {
	context := testsite.CreateContext(t)
	query_done := false
	go func() {
		query := <-context.Search
		query.Result <- []*search.SearchResult{}
		query_done = true
	}()
	testsite.DoVerifyRequest(context, "/search?q=query", http.StatusOK)
	if !query_done {
		t.Errorf("Search query was not passed into search routine")
	}
}

func return_query_result(context testsite.TestSiteContext) func(*bool) {
	return func(query_done *bool) {
		year := context.State.Years[0]
		section := year.Sections[0]
		entry := section.Entries[0]
		query := <-context.Search
		query.Result <- []*search.SearchResult{
			{
				Year:    year,
				Section: section,
				Entry:   entry,
			},
		}
		*query_done = true
	}
}

func TestSearchPageWithOneResultShouldRenderWithoutErrors(t *testing.T) {
	context := testsite.CreateContext(t)
	query_done := false
	go return_query_result(context)(&query_done)
	testsite.DoVerifyRequest(context, "/search?q=query", http.StatusOK)
	if !query_done {
		t.Errorf("Search query was not passed into search routine")
	}
}

func TestSearchPageWithAllResultsShouldRenderWithoutErrors(t *testing.T) {
	context := testsite.CreateContext(t)
	query_done := false
	cache := search.InitSearch(context.State)
	go func() {
		search.DoSearch(context.State, context.Search, cache)
		query_done = true
	}()
	testsite.DoVerifyRequest(context, "/search?q=entry", http.StatusOK)
	if !query_done {
		t.Errorf("Search query was not passed into search routine")
	}
}

func TestNotfoundPageShouldRenderWithoutErrors(t *testing.T) {
	context := testsite.CreateContext(t)
	testsite.DoVerifyRequest(context, "/not-found", http.StatusNotFound)
	testsite.DoVerifyRequest(context, "/2000/not-found", http.StatusNotFound)
	testsite.DoVerifyRequest(context, "/2000/section-1/not-found", http.StatusNotFound)
	testsite.DoVerifyRequest(context, "/2000/section-1/entry-1-by-author-1/not-found", http.StatusNotFound)
}

func verify_2_responses(
	context testsite.TestSiteContext,
	path_first string,
	path_second string,
	wanted_code int) ([]byte, []byte) {
	rr_first := testsite.DoVerifyRequest(context, path_first, wanted_code)
	result_first := []byte{}
	{
		var err error
		if result_first, err = io.ReadAll(rr_first.Body); err != nil {
			context.T.Error(err)
		}
		if len(result_first) == 0 {
			context.T.Errorf("Result to path %s returned no data", path_first)
		}
	}
	rr_second := testsite.DoVerifyRequest(context, path_second, wanted_code)
	result_second := []byte{}
	{
		var err error
		if result_second, err = io.ReadAll(rr_second.Body); err != nil {
			context.T.Error(err)
		}
		if len(result_second) == 0 {
			context.T.Errorf("Result to path %s returned no data", path_second)
		}
	}
	return result_first, result_second
}

func TestSectionPageShouldRenderDeterministically(t *testing.T) {
	context := testsite.CreateContext(t)
	result_first, result_second := verify_2_responses(
		context,
		"/2000/section-1",
		"/2000/section-1",
		http.StatusOK)
	if !bytes.Equal(result_first, result_second) {
		t.Errorf("2 subsequent responses to section are not equal!")
	}
}

func TestEntryPageShouldRenderDeterministically(t *testing.T) {
	context := testsite.CreateContext(t)
	result_first, result_second := verify_2_responses(
		context,
		"/2000/section-1/entry-1-by-author-1",
		"/2000/section-1/entry-1-by-author-1",
		http.StatusOK)
	if !bytes.Equal(result_first, result_second) {
		t.Errorf("2 subsequent responses to entry are not equal!")
	}
}

func TestSectionOffsetsShouldGiveDifferentResults(t *testing.T) {
	context := testsite.CreateContext(t)
	result_first, result_second := verify_2_responses(
		context,
		"/2000/section-1?offset=0",
		fmt.Sprintf(
			"/2000/section-1?offset=%d",
			site.MAX_SECTION_DISPLAY_ENTRIES),
		http.StatusOK)
	if bytes.Equal(result_first, result_second) {
		t.Errorf("Sections with offset requests are not different!")
	}
}

func TestSearchInJsonFormatShouldReturnJson(t *testing.T) {
	context := testsite.CreateContext(t)
	rr := testsite.DoVerifyRequest(context, "/search?q=&fmt=json", http.StatusOK)
	body, err := io.ReadAll(rr.Body)
	if err != nil {
		t.Error(err)
	}
	if !json.Valid(body) {
		t.Errorf("JSON search format did not return valid JSON: %s", string(body)[:40])
	}
}

func TestResultReturningSearchInJsonFormatShouldReturnJson(t *testing.T) {
	context := testsite.CreateContext(t)
	query_done := false
	go return_query_result(context)(&query_done)
	rr := testsite.DoVerifyRequest(context, "/search?q=query&fmt=json", http.StatusOK)
	if !query_done {
		t.Errorf("Search query was not passed into search routine")
	}
	body, err := io.ReadAll(rr.Body)
	if err != nil {
		t.Error(err)
	}
	if !json.Valid(body) {
		t.Errorf("JSON search format did not return valid JSON: %s", string(body)[:40])
	}
}
