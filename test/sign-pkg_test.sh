#!/usr/bin/env bash

set -xeuo pipefail

function skip_test()
{
    {
        local name=$(basename "$0")
        cat <<EOF
<testsuite errors="0" tests="1" failures="0" skipped="1" time="0">
    <testcase classname="sign" name="$name" time="0.0">
        <skipped/>
    </testcase>
</testsuite>
EOF
    } > "$XML_OUTPUT_FILE"
    exit 0
}

echo >&2 "Creating signing keys"
# This can fail with LibreSSL 2.8.3 or other version used on
# macOS. Skip the test in that case.
openssl genpkey \
        -algorithm EC \
        -pkeyopt ec_paramgen_curve:secp521r1 \
        -pkeyopt ec_param_enc:named_curve \
        -out private.pem || skip_test
openssl pkey -in private.pem -pubout -out public.pem

echo >&2 "Signing package"
./sign-pkg private.pem signature.sig

echo >&2 "Verifying package"
openssl dgst \
    -verify public.pem \
    -signature signature.sig \
    assembly-archive-pkg.tar
