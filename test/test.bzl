def _pkg_tar_extracted_impl(ctx):
    out = ctx.actions.declare_directory(ctx.label.name)
    ctx.actions.run(
        outputs = [out],
        inputs = [ctx.file.src],
        executable = "tar",
        arguments = [
            "xf",
            ctx.file.src.path,
            "-C",
            out.path,
            "--strip-components=1",
        ],
    )
    return [DefaultInfo(files = depset([out]))]

pkg_tar_extracted = rule(
    implementation = _pkg_tar_extracted_impl,
    attrs = {
        "src": attr.label(allow_single_file = True),
    },
)
