package site_links_test

import (
	"bytes"
	"fmt"
	"golang.org/x/net/html"
	"io"
	"net/http"
	"path"
	"strings"
	"testing"

	"asma/search"
	"asma/site"
	"asma/testsite"
)

func get_relative_links(body []byte, page_path string) []string {
	tokens := html.NewTokenizer(bytes.NewReader(body))
	result := []string{}

tokens_loop:
	for {
		token_type := tokens.Next()
		switch token_type {
		case html.ErrorToken:
			break tokens_loop
		case html.StartTagToken:
			token := tokens.Token()
			if token.Data != "a" {
				continue
			}
			for _, attr := range token.Attr {
				if attr.Key != "href" {
					continue
				}
				value := attr.Val
				if len(value) == 0 {
					break
				}
				if strings.Contains(value, ":") {
					break
				}
				if value[0] == '/' {
					result = append(result, path.Clean(value))
					break
				}
				if value[0] == '?' {
					result = append(result, page_path+value)
					break
				}
				result = append(
					result, path.Clean(path.Join(page_path, value)))
				break
			}
		}
	}
	return result
}

func check_all_page_links(
	context testsite.TestSiteContext, page_path string, wanted_code int) {
	rr := testsite.DoVerifyRequest(context, page_path, wanted_code)
	body := []byte{}
	var err error
	if body, err = io.ReadAll(rr.Body); err != nil {
		context.T.Error(err)
	}
	if len(body) == 0 {
		context.T.Errorf("Body of path %s returned no data", page_path)
	}
	links := get_relative_links(body, page_path)
	for _, link_path := range links {
		testsite.DoVerifyRequest(context, link_path, http.StatusOK)
	}
}

func TestMainPageLinksShouldAllWork(t *testing.T) {
	context := testsite.CreateContext(t)
	check_all_page_links(context, "/", http.StatusOK)
}

func TestYearLinksShouldAllWork(t *testing.T) {
	context := testsite.CreateContext(t)
	check_all_page_links(context, "/2000", http.StatusOK)
}

func TestSectionLinksShouldAllWork(t *testing.T) {
	context := testsite.CreateContext(t)
	check_all_page_links(context, "/2000/section-1", http.StatusOK)
	check_all_page_links(
		context,
		fmt.Sprintf(
			"/2000/section-1?offset=%d",
			site.MAX_SECTION_DISPLAY_ENTRIES),
		http.StatusOK)
}

func TestEntryLinksShouldAllWork(t *testing.T) {
	context := testsite.CreateContext(t)
	check_all_page_links(context, "/2000/section-1/entry-1-by-author-1", http.StatusOK)
}

func TestEmptySearchLinksShouldAllWork(t *testing.T) {
	context := testsite.CreateContext(t)
	check_all_page_links(context, "/search", http.StatusOK)
}

func TestResultReturningSearchLinksShouldAllWork(t *testing.T) {
	context := testsite.CreateContext(t)
	cache := search.InitSearch(context.State)
	go func() {
		for {
			search.DoSearch(context.State, context.Search, cache)
		}
	}()
	check_all_page_links(context, "/search?q=entry", http.StatusOK)
}

func TestNotFoundLinksSHouldAllWork(t *testing.T) {
	context := testsite.CreateContext(t)
	check_all_page_links(context, "/not-found", http.StatusNotFound)
	check_all_page_links(context, "/2000/not-found", http.StatusNotFound)
	check_all_page_links(context, "/2000/section-1/not-found", http.StatusNotFound)
	check_all_page_links(context, "/2000/section-1/entry-1-by-author-1/not-found", http.StatusNotFound)
}
