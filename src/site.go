package site

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"html"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"text/template"
	"time"

	"asma/base"
	"asma/search"
	"asma/server"
	"asma/state"
)

const MAX_SECTION_DISPLAY_ENTRIES = 30
const MAX_PREVIEW_ENTRIES = 5

const YEARLY_NAVIGATION_YEARS = 7

const CACHE_TIME_STATIC_PAGE_S = 120
const CACHE_TIME_DYNAMIC_PAGE_S = 30

type PageFormat int

const (
	FMT_HTML PageFormat = iota
	FMT_JSON PageFormat = iota
)

type SiteTemplates struct {
	Main        *template.Template
	Year        *template.Template
	Section     *template.Template
	Entry       *template.Template
	NotFound    *template.Template
	Description *template.Template
	Search      *template.Template
	Overload    *template.Template
	Meatadata   *template.Template
}

type Site struct {
	Settings  base.SiteSettings
	State     *state.SiteState
	Templates *SiteTemplates
	Static    map[string]string
	Search    chan search.SearchQuery
}

func (site *Site) AddPrefix(path string) string {
	return fmt.Sprintf("%s/%s", site.Settings.SiteRoot, path)
}

type YearlyNavigation struct {
	Years        []InternalLink
	CurrentIndex int
}

type Breadcrumbs struct {
	Parents []InternalLink
	Last    InternalLink
}

type PageNavigation struct {
	Prev InternalLink
	Next InternalLink
}

type Prefetch struct {
	Path string
	Type string
}

type PageContext struct {
	Path             string
	Breadcrumbs      Breadcrumbs
	Title            string
	Description      string
	Site             Site
	Static           map[string]string
	CurrentYear      int
	Query            string
	Prefetches       []Prefetch
	SiteState        *state.SiteState
	Navigation       PageNavigation
	YearlyNavigation YearlyNavigation
}

type DisplayEntry struct {
	Year    *base.Year
	Section *base.Section
	Entry   *base.Entry
}

type GalleryThumbnails struct {
	Path           string
	Title          string
	GalleryEntries []DisplayEntry
}

type InternalLink struct {
	Path     string
	Contents string
	Title    string
}

type YearInfo struct {
	Prev *base.Year
	Curr *base.Year
	Next *base.Year
}

type EntryInfo struct {
	Year    *base.Year
	Section *base.Section
	Prev    *base.Entry
	Curr    *base.Entry
	Next    *base.Entry
}

type DisplayEntries struct {
	Context             *PageContext
	Row                 int
	ShowThumbnailHeader bool
	Entries             []DisplayEntry
}

type SiteThumbnails struct {
	Site       Site
	Thumbnails *base.Thumbnails
}

type SiteEntry struct {
	Site    Site
	Year    *base.Year
	Section *base.Section
	Entry   *base.Entry
}

type EntryContext struct {
	Year    *base.Year
	Section *base.Section
	Entry   EntryInfo
	Asset   string
	Context PageContext
}

type SiteImageInfo struct {
	Site Site
	Info base.ImageInfo
}

func (image SiteImageInfo) GetPath() string {
	return image.Site.AddPrefix(image.Info.Path + "?" + image.Info.Checksum)
}

func (info SiteImageInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Path string          `json:"path"`
		Size base.Resolution `json:"size"`
		Type string          `json:"type"`
	}{
		Path: info.GetPath(),
		Size: info.Info.Size,
		Type: info.Info.Type,
	})
}

func (thumbnails SiteThumbnails) MarshalJSON() ([]byte, error) {
	site_images := make([]SiteImageInfo, len(thumbnails.Thumbnails.Sources))
	for i, image := range thumbnails.Thumbnails.Sources {
		site_images[i] = SiteImageInfo{
			Site: thumbnails.Site,
			Info: image,
		}
	}
	return json.Marshal(struct {
		Default SiteImageInfo   `json:"default"`
		Sources []SiteImageInfo `Json:"sources"`
	}{
		Default: SiteImageInfo{
			Site: thumbnails.Site,
			Info: thumbnails.Thumbnails.Default,
		},
		Sources: site_images,
	})
}

func (entry SiteEntry) GetPath() string {
	return entry.Site.AddPrefix(entry.Entry.Path)
}

func (entry SiteEntry) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Year       int            `json:"year"`
		Section    string         `json:"section"`
		Path       string         `json:"path"`
		Title      string         `json:"title"`
		Author     string         `json:"author"`
		Thumbnails SiteThumbnails `json:"thumbnails"`
	}{
		Year:    entry.Year.Year,
		Section: entry.Section.Name,
		Path:    entry.GetPath(),
		Title:   entry.Entry.Title,
		Author:  entry.Entry.Author,
		Thumbnails: SiteThumbnails{
			Site:       entry.Site,
			Thumbnails: &entry.Entry.Thumbnails,
		},
	})
}

type NotFoundContext struct {
	Parent  string
	Context PageContext
}

func in_array(array []DisplayEntry, entry *base.Entry) bool {
	for _, array_entry := range array {
		if array_entry.Entry.Path == entry.Path {
			return true
		}
	}
	return false
}

func view_get_image_data_src(image base.ImageInfo) string {
	data, err := os.ReadFile(image.FsPath)
	// This should basically lead into 404 error:
	if err != nil {
		return fmt.Sprintf(
			"%s?%s", html.EscapeString(image.Path), image.Checksum)
	}
	return fmt.Sprintf(
		"data:image/%s;base64,%s",
		image.Type,
		base64.StdEncoding.EncodeToString(data))
}

func struct_display_entries(
	context PageContext, row int, thumbnails []DisplayEntry) DisplayEntries {
	return DisplayEntries{
		Context:             &context,
		Row:                 row,
		ShowThumbnailHeader: false,
		Entries:             thumbnails,
	}
}

func random_select_section_entries(
	prng *rand.Rand,
	year *base.Year,
	section *base.Section,
	amount int) []DisplayEntry {
	section_indexes := prng.Perm(len(section.Entries))
	max_items := len(section.Entries)
	if amount < max_items {
		max_items = amount
	}
	result := make([]DisplayEntry, max_items)
	for i := 0; i < max_items; i++ {
		result[i] = DisplayEntry{
			Year:    year,
			Section: section,
			Entry:   section.Entries[section_indexes[i]],
		}
	}
	return result
}

func get_page_format(r *http.Request) PageFormat {
	format_str := string(r.FormValue("fmt"))
	if strings.ToLower(format_str) == "json" {
		return FMT_JSON
	}
	return FMT_HTML
}

// Adds a short cache control header value. This is used for pages
// that can change, but will very likely not change unless the site
// layout or page contents is updated.
func add_cache_time(w http.ResponseWriter, seconds int) {
	header := w.Header()
	header.Add("Cache-Control", fmt.Sprintf("public, max-age=%d", seconds))
}

func author_title(entry base.Entry) string {
	var author_title string
	if entry.Title == "" {
		return ""
	} else if entry.Author == "" {
		author_title = entry.Title
	} else {
		author_title = entry.Title + " by " + entry.Author
	}
	return author_title
}

func view_site_root(context PageContext, path string) string {
	return html.EscapeString(context.Site.AddPrefix(path))
}

func view_author_title(entry base.Entry) string {
	return html.EscapeString(author_title(entry))
}

var WORDS_MATCH = regexp.MustCompile("(\\w+)|(\\W+)")

// We limit the length of displayable names in the user interface to a
// maximum length. In addition we ensure that there are word breaks in
// middle of really long words to prevent wrap bombs. This function
// does these limiting and wrapping operations and adds "..." to the
// end of overly long strings.
func view_cut_string(target string, max_length int) string {
	MAX_WORD_LENGTH := 20
	if max_length < MAX_WORD_LENGTH {
		MAX_WORD_LENGTH = max_length
	}
	if len(target) <= MAX_WORD_LENGTH {
		return target
	}
	words := WORDS_MATCH.FindAllString(target, -1)
	var buffer bytes.Buffer
	for _, word := range words {
		for len(word) > MAX_WORD_LENGTH {
			buffer.WriteString(word[:MAX_WORD_LENGTH])
			buffer.WriteString("\u200B")
			word = word[MAX_WORD_LENGTH:]
		}
		buffer.WriteString(word)
	}
	word_cut_data := buffer.String()
	if len(word_cut_data) < max_length {
		return word_cut_data
	}
	return strings.TrimSpace(word_cut_data[:max_length-2]) + "\u2026"
}

func view_attribute(name string, value string) string {
	if len(value) == 0 {
		return ""
	}
	return name + "=\"" + html.EscapeString(value) + "\""
}

func view_image_srcset(context PageContext, images []base.ImageInfo) string {
	type SrcSet struct {
		Srcs  bytes.Buffer
		Sizes bytes.Buffer
	}
	sets := map[string]*SrcSet{}
	var last_set *SrcSet = nil
	last_type := ""
	site_root_html := []byte(html.EscapeString(context.Site.AddPrefix("")))
	for _, image := range images {
		srcset := last_set
		if image.Type != last_type {
			var ok bool
			srcset, ok = sets[image.Type]
			if !ok {
				srcset = &SrcSet{}
				sets[image.Type] = srcset
				srcset.Srcs.Grow(768)
				srcset.Sizes.Grow(64)
			} else {
				srcset.Srcs.WriteString(", ")
				srcset.Sizes.WriteString(", ")
			}
			last_set = srcset
			last_type = image.Type
		} else {
			srcset.Srcs.WriteString(", ")
			srcset.Sizes.WriteString(", ")
		}
		srcset.Srcs.Write(site_root_html)
		srcset.Srcs.WriteString(
			html.EscapeString(strings.Replace(image.Path, " ", "%20", -1)))
		srcset.Srcs.Write([]byte("?"))
		srcset.Srcs.WriteString(html.EscapeString(image.Checksum))
		srcset.Srcs.Write([]byte(" "))
		size_x_str := []byte(strconv.Itoa(image.Size.X))
		srcset.Srcs.Write(size_x_str)
		srcset.Srcs.Write([]byte("w"))
		srcset.Sizes.Write(size_x_str)
		srcset.Sizes.Write([]byte("px"))
	}
	result := ""
	for set_type, set_value := range sets {
		result += fmt.Sprintf(
			"<source type='%s' srcset='%s' sizes='%s' />",
			set_type,
			set_value.Srcs.String(),
			set_value.Sizes.String(),
		)
	}
	return result
}

func add_prefetch_links(context *PageContext) {
	var result []Prefetch
	if len(context.Navigation.Prev.Path) > 0 {
		result = append(result, Prefetch{
			Path: context.Navigation.Prev.Path,
			Type: "document",
		})
	}
	if len(context.Navigation.Next.Path) > 0 {
		result = append(result, Prefetch{
			Path: context.Navigation.Next.Path,
			Type: "document",
		})
	}
	context.Prefetches = result
}

func mod_context_no_breadcrumbs(context PageContext) PageContext {
	no_breadcrumbs := context
	no_breadcrumbs.Breadcrumbs = Breadcrumbs{}
	return no_breadcrumbs
}

func mod_context_replace_navigation(context PageContext, navigation PageNavigation) PageContext {
	replaced := context
	replaced.Navigation = navigation
	return replaced
}

type GalleryRenderer struct {
	Settings *base.SiteSettings
	Template *template.Template
}

func load_template(
	settings *base.SiteSettings,
	name string,
	filename string,
	clonable *template.Template) (*template.Template, error) {
	t := template.New(name)
	if clonable != nil {
		cloned, err := clonable.Clone()
		if err != nil {
			return nil, err
		}
		t = cloned.New(name)
	}
	template_data, data_err := os.ReadFile(
		path.Join(settings.TemplatesDir, filename))
	if data_err != nil {
		return nil, data_err
	}
	t_read, parse_err := t.Parse(string(template_data))
	if parse_err != nil {
		return nil, parse_err
	}
	return t_read, nil
}

func create_base_template(name string) *template.Template {
	t := template.New(name)
	functions := template.FuncMap{}
	functions["view_site_root"] = view_site_root
	functions["view_author_title"] = view_author_title
	functions["view_cut_string"] = view_cut_string
	functions["view_attribute"] = view_attribute
	functions["mod_context_no_breadcrumbs"] = mod_context_no_breadcrumbs
	functions["mod_context_replace_navigation"] = mod_context_replace_navigation
	functions["view_get_image_data_src"] = view_get_image_data_src
	functions["struct_display_entries"] = struct_display_entries
	functions["view_image_srcset"] = view_image_srcset
	return t.Funcs(functions)
}

func load_templates(settings *base.SiteSettings) (SiteTemplates, error) {
	var templates SiteTemplates
	data := "asdf"

	generic := create_base_template("generic")

	var err error
	{
		generic = template.Must(
			load_template(settings, "thumbnails", "thumbnails.html.tmpl", generic))
		generic = template.Must(
			load_template(settings, "breadcrumbs", "breadcrumbs.html.tmpl", generic))
		generic = template.Must(
			load_template(settings, "navbar", "navbar.html.tmpl", generic))
		generic = template.Must(
			load_template(settings, "yearlynavigation", "yearlynavigation.html.tmpl", generic))
	}
	{
		contents := template.Must(
			load_template(settings, "page-contents", "main.html.tmpl", generic))
		templates.Main, err = load_template(
			settings, "main", "layout.html.tmpl", contents)
		if err != nil {
			return templates, err
		}
	}
	{
		contents := template.Must(
			load_template(settings, "page-contents", "year.html.tmpl", generic))
		templates.Year, err = load_template(
			settings, "year", "layout.html.tmpl", contents)
		if err != nil {
			return templates, err
		}
	}
	{
		contents := template.Must(
			load_template(settings, "page-contents", "section.html.tmpl", generic))
		templates.Section, err = load_template(
			settings, "section", "layout.html.tmpl", contents)
		if err != nil {
			return templates, err
		}
	}
	{
		contents := template.Must(
			load_template(settings, "page-contents", "entry.html.tmpl", generic))
		contents = template.Must(
			load_template(settings, "metadata", "entry-metadata.html.tmpl", contents))
		templates.Entry, err = load_template(
			settings, "entry", "layout.html.tmpl", contents)
		if err != nil {
			return templates, err
		}
	}
	{
		contents := template.Must(
			load_template(settings, "page-contents", "search.html.tmpl", generic))
		templates.Search, err = load_template(
			settings, "search", "layout.html.tmpl", contents)
		if err != nil {
			return templates, err
		}
	}
	{
		contents := template.Must(
			load_template(settings, "page-contents", "404.html.tmpl", generic))
		templates.NotFound, err = load_template(
			settings, "404", "layout.html.tmpl", contents)
		if err != nil {
			return templates, err
		}
	}
	{
		contents := template.Must(
			load_template(settings, "page-contents", "overload.html.tmpl", generic))
		templates.Overload, err = load_template(
			settings, "search", "layout.html.tmpl", contents)
		if err != nil {
			return templates, err
		}
	}
	{
		t := template.New("description")
		templates.Description = template.Must(t.Parse(data))
	}
	return templates, nil
}

func walk_directories_impl(root string, walkFn filepath.WalkFunc, subpath string, current_depth int, err error) error {
	if err != nil {
		return err
	}
	if current_depth > 8 {
		return errors.New(
			fmt.Sprintf(
				"Trying to recurse too deep under %s at %s!", root, subpath))
	}
	files, err_readdir := os.ReadDir(path.Join(root, subpath))
	if err_readdir != nil {
		return err_readdir
	}
	for _, file := range files {
		new_subpath := path.Join(subpath, file.Name())
		info, err_info := file.Info()
		if err_info != nil {
			return errors.New(
				fmt.Sprintf(
					"Unable to get info for path %s: %s",
					new_subpath,
					err_info))
		}
		if info.IsDir() {
			err_walk := walk_directories_impl(
				root, walkFn, new_subpath, current_depth+1, nil)
			if err_walk != nil {
				return err_walk
			}
			continue
		}
		if err := walkFn(new_subpath, info, nil); err != nil {
			return err
		}
	}
	return nil
}

func walk_directories(root string, walkFn filepath.WalkFunc) error {
	return walk_directories_impl(root, walkFn, "", 0, nil)
}

func load_static_files(settings *base.SiteSettings) (map[string]string, error) {
	result := make(map[string]string)
	err := walk_directories(
		settings.StaticDir,
		func(subpath string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			fs_path := path.Join(settings.StaticDir, subpath)
			checksum, err_checksum := base.CreateFileChecksum(fs_path)
			if err_checksum != nil {
				log.Printf("Checksum calculation failed on %s", fs_path)
				return err_checksum
			}
			result[subpath] = checksum
			return nil
		},
	)
	return result, err
}

func render_template(
	w http.ResponseWriter,
	t *template.Template,
	data interface{}) error {
	wr := bufio.NewWriterSize(w, 1024*64)
	err := t.Execute(wr, data)
	if err == nil {
		err = wr.Flush()
		if err == nil {
			return nil
		}
	}
	// Ignore write errors, as they are likely due to client closing
	// connection mid-write.
	switch err.(type) {
	case template.ExecError:
		return err
	default:
		return nil
	}
}

func render_json(w http.ResponseWriter, data []byte) error {
	w.Header().Add("Content-Type", "application/json")
	written, err := w.Write(data)
	if err != nil {
		log.Printf(
			"Failed to write JSON response. Only wrote %d/%d bytes: %s",
			written, len(data), err)
	}
	return err
}

type RequestHandlerFunc func(
	site Site,
	path_elements map[string]string,
	w http.ResponseWriter,
	r *http.Request)

type OffsetItems struct {
	Offset int
	Items  int
}

type EntryOffsets struct {
	TotalItems    int
	CurrentOffset int
	ItemsStart    int
	ItemsEnd      int
	Prev          *OffsetItems
	Next          *OffsetItems
}

func create_offset_navigation(
	total_entries int, offset_str string, max_entries int) EntryOffsets {
	if offset_str == "" {
		offset_str = "0"
	}
	offset, err_offset := strconv.Atoi(offset_str)
	if err_offset != nil {
		log.Println(err_offset)
		offset = 0
	}
	if offset < 0 {
		offset = 0
	} else if offset%max_entries != 0 {
		offset = 0
	}

	result := EntryOffsets{
		CurrentOffset: offset,
		TotalItems:    total_entries,
	}

	prev_offset := offset - max_entries
	if prev_offset < 0 {
		prev_offset = 0
	}
	next_offset := offset + max_entries

	if total_entries <= max_entries {
		result.ItemsStart = 0
		result.ItemsEnd = total_entries
	} else {
		if offset+max_entries < total_entries {
			result.ItemsStart = offset
			result.ItemsEnd = offset + max_entries
		} else if offset < total_entries {
			result.ItemsStart = offset
			result.ItemsEnd = total_entries
		}
	}

	if (prev_offset == 0 && offset != 0) || prev_offset > 0 {
		result.Next = &OffsetItems{
			Offset: prev_offset,
			Items:  max_entries,
		}
	}
	if next_offset < total_entries {
		next_entries_count := total_entries - next_offset
		if max_entries < next_entries_count {
			next_entries_count = max_entries
		}
		result.Prev = &OffsetItems{
			Offset: next_offset,
			Items:  next_entries_count,
		}
	}
	return result
}

func _resolve_link(
	site Site,
	path_elements map[string]string,
	link_root string,
	link_type string) (*string, error) {
	link_name, ok_type := path_elements[link_type]
	if !ok_type {
		return &link_root, errors.New("No type exists")
	}

	potential_link := path.Join(site.Settings.DataDir, link_root, link_name)
	target, err_lstat := os.Lstat(potential_link)
	if err_lstat != nil {
		return nil, errors.New("No file exists")
	}
	if target.Mode()&os.ModeSymlink != 0 {
		link_target, err_link := os.Readlink(potential_link)
		if err_link != nil {
			link_read_error := fmt.Sprintf(
				"Unreadable symbolic link %s: %s",
				potential_link,
				err_link)
			log.Println(link_read_error)
			return nil, errors.New(link_read_error)
		}
		if strings.Contains(link_target, "..") {
			log.Println(fmt.Sprintf(
				"Relative link %s -> %s", potential_link, link_target))
			return nil, errors.New("Relative links are not supported")
		}
		if strings.HasPrefix(link_target, "/") {
			log.Println(fmt.Sprintf(
				"Absolute link %s -> %s", potential_link, link_target))
			return nil, errors.New("Absolute links are not supported")
		}
		result := path.Join(link_root, link_target)
		return &result, nil
	}
	result := path.Join(link_root, link_name)
	return &result, nil
}

func get_redirect(site Site, path_elements map[string]string) (*string, error) {
	link_root := ""
	new_year, err_year := _resolve_link(
		site, path_elements, link_root, "Year")
	if err_year != nil {
		return nil, errors.New("No year exists")
	}
	if new_year != nil {
		link_root = *new_year
	} else {
		link_root = path.Join(link_root, path_elements["Year"])
	}

	new_section, err_section := _resolve_link(
		site, path_elements, link_root, "Section")
	if err_section != nil {
		if new_section != nil {
			result := site.AddPrefix(link_root)
			return &result, nil
		}
		return nil, errors.New("No section exists")
	}
	if new_section != nil {
		link_root = *new_section
	} else {
		link_root = path.Join(link_root, path_elements["Section"])
	}

	new_entry, err_entry := _resolve_link(
		site, path_elements, link_root, "Entry")
	if err_entry != nil {
		if new_entry != nil {
			result := site.AddPrefix(link_root)
			return &result, nil
		}
		return nil, errors.New("No element exists")
	}
	if new_entry != nil {
		result := site.AddPrefix(*new_entry)
		return &result, nil
	}
	return nil, errors.New("404")
}
func get_year_info(site Site, path_elements map[string]string) (YearInfo, error) {
	info := YearInfo{}
	requested_year, year_err := strconv.Atoi(path_elements["Year"])
	if year_err != nil {
		return info, errors.New(
			fmt.Sprintf(
				"Invalid requested year: %s: %s",
				path_elements["Year"],
				year_err))
	}
	last_index := 0
	for i, candidate_year := range site.State.Years {
		last_index = i
		if candidate_year.Year == requested_year {
			info.Curr = candidate_year
			break
		}
		info.Next = candidate_year
	}
	if info.Curr.NotNil().Key == "" {
		return info, errors.New(
			fmt.Sprintf("Year %s not found!", path_elements["Year"]))
	}
	if last_index+1 < len(site.State.Years) {
		info.Prev = site.State.Years[last_index+1]
	}
	return info, nil
}

func get_section_info(site Site, path_elements map[string]string) (SectionInfo, error) {
	info := SectionInfo{}
	year, year_err := get_year_info(site, path_elements)
	if year_err != nil {
		return info, year_err
	}
	info.Year = year.Curr
	key := path_elements["Section"]
	last_index := 0
	for i, candidate := range year.Curr.Sections {
		last_index = i
		if candidate.Key == key {
			info.Curr = candidate
			break
		}
		info.Next = candidate
	}
	if info.Curr == nil {
		return info, errors.New(
			fmt.Sprintf("Section %s not found!", key))
	}
	if last_index+1 < len(year.Curr.Sections) {
		info.Prev = year.Curr.Sections[last_index+1]
	}
	return info, nil
}

func get_yearly_navigation(site Site, current_year int) YearlyNavigation {
	if len(site.State.Years) == 0 {
		return YearlyNavigation{}
	}
	year_max := site.State.Years[0].Year
	year_min := site.State.Years[len(site.State.Years)-1].Year
	years_count := len(site.State.Years)
	highlighted_index := -1
	if current_year < year_min {
		highlighted_index = -1
	} else if year_max < current_year {
		highlighted_index = -1
	} else {
		highlighted_index = year_max - current_year
	}

	visible_years := YEARLY_NAVIGATION_YEARS
	if highlighted_index < int(YEARLY_NAVIGATION_YEARS/2)+2 {
		visible_years++
	}
	if years_count-highlighted_index < int(YEARLY_NAVIGATION_YEARS/2)+1 {
		visible_years++
	}

	index_first := highlighted_index - visible_years/2
	index_last := index_first + visible_years
	if index_last >= years_count {
		index_last = years_count
		index_first = index_last - visible_years
	}
	if index_first < 0 {
		index_first = 0
		index_last = index_first + visible_years
		if index_last >= years_count {
			index_last = years_count
		}
	}
	var display_years []InternalLink
	if 0 < index_first {
		laquo := InternalLink{
			Path:     site.AddPrefix(site.State.Years[index_first-1].Path),
			Contents: "«",
			Title:    site.State.Years[index_first-1].Key,
		}
		display_years = append(display_years, laquo)
	}

	current_index := -1
	for i := index_first; i < index_last; i++ {
		year_link := InternalLink{
			Path:     site.AddPrefix(site.State.Years[i].Path),
			Contents: fmt.Sprintf("'%02d", (site.State.Years[i].Year % 100)),
			Title:    site.State.Years[i].Key,
		}
		if i == highlighted_index {
			current_index = len(display_years)
		}
		display_years = append(display_years, year_link)
	}

	if index_last < years_count {
		raquo := InternalLink{
			Path:     site.AddPrefix(site.State.Years[index_last].Path),
			Contents: "»",
			Title:    site.State.Years[index_last].Key,
		}
		display_years = append(display_years, raquo)
	}

	return YearlyNavigation{
		Years:        display_years,
		CurrentIndex: current_index,
	}
}

/**
 * Creates a random number generator that returns the same values while
 * page cache time is valid.
 *
 * This is relevant when trying to avoid excessive disk access on pages
 * that display random selection of thumbnails.
 */
func get_current_page_prng(cache_time_s int) *rand.Rand {
	now := time.Now()
	seed_raw := now.Unix()
	seed := seed_raw - seed_raw%int64(cache_time_s)
	return rand.New(rand.NewSource(seed))
}

func _create_year_range_link(
	site Site,
	years []*base.Year,
	latest_year *base.Year) InternalLink {
	if len(years) == 0 {
		return InternalLink{}
	}
	if len(years) == 1 {
		return InternalLink{
			Path: fmt.Sprintf(
				"%s?y=%d", site.AddPrefix(""), years[0].Year),
			Contents: fmt.Sprintf("%d", years[0].Year),
		}
	}
	years_first := years[len(years)-1]
	years_last := years[0]
	if years_last == latest_year {
		return InternalLink{
			Path: site.AddPrefix(""),
			Contents: fmt.Sprintf(
				"%d-%d", years_first.Year, years_last.Year),
		}
	}
	return InternalLink{
		Path: fmt.Sprintf(
			"%s?y=%d", site.AddPrefix(""), years_first.Year),
		Contents: fmt.Sprintf("%d-%d", years_first.Year, years_last.Year),
	}
}

func _read_year_range(
	site Site, r *http.Request) ([]*base.Year, *InternalLink, *InternalLink) {
	year_start := 0
	year_end := 99999
	year_start_requested := string(r.FormValue("y"))
	if len(year_start_requested) > 0 {
		var err error
		year_start, err = strconv.Atoi(year_start_requested)
		if err == nil {
			year_end = year_start + DEFAULT_MAIN_YEARS
		}
	} else if len(site.State.Years) > 0 {
		// Years array is sorted in the reverse order.
		year_end = site.State.Years[0].Year
		year_start = year_end - DEFAULT_MAIN_YEARS
	}
	max_year := year_end + 1 + DEFAULT_MAIN_YEARS
	min_year := year_start - 1 - DEFAULT_MAIN_YEARS

	var years_before []*base.Year
	var years []*base.Year
	var years_after []*base.Year
	for _, year := range site.State.Years {
		if max_year < year.Year {
			continue
		}
		if year.Year < min_year {
			continue
		}
		if year.Year < year_start {
			years_before = append(years_before, year)
		} else if year_end < year.Year {
			years_after = append(years_after, year)
		} else {
			years = append(years, year)
		}
	}

	var latest_year *base.Year
	if len(site.State.Years) > 0 {
		latest_year = site.State.Years[0]
	}
	link_before := _create_year_range_link(site, years_before, latest_year)
	link_after := _create_year_range_link(site, years_after, latest_year)
	return years, &link_before, &link_after
}

type RequestHandler struct {
	regex    *regexp.Regexp
	callback RequestHandlerFunc
}

var HANDLERS = []RequestHandler{
	{regexp.MustCompile(`^(?P<Year>\d{4})/(?P<Section>[a-z0-9\-]+)/(?P<Entry>[a-z0-9\-]+)/?$`),
		handle_entry},
	{regexp.MustCompile(`^(?P<Year>\d{4})/(?P<Section>[a-z0-9\-]+)/?$`), handle_section},
	{regexp.MustCompile(`^search/?$`), handle_search},
	{regexp.MustCompile(`^(?P<Year>\d{4})/?$`), handle_year},
	{regexp.MustCompile("^$"), handle_main},
}

func handle_not_found(
	site Site,
	w http.ResponseWriter,
	r *http.Request) {
	page_context := PageContext{
		Path:             site.AddPrefix(r.URL.EscapedPath()),
		Title:            "404 page not found",
		Description:      "404 page not found",
		Site:             site,
		Static:           site.Static,
		YearlyNavigation: get_yearly_navigation(site, 0),
	}
	context := NotFoundContext{
		Parent:  site.AddPrefix(path.Dir(r.URL.EscapedPath())),
		Context: page_context,
	}
	w.WriteHeader(http.StatusNotFound)
	err_template := render_template(w, site.Templates.NotFound, context)
	if err_template != nil {
		log.Printf("Internal 404 page error: %v", err_template)
		server.Ise(w)
	}
}

func route_request(
	site Site,
	w http.ResponseWriter,
	r *http.Request) {
	path := r.URL.EscapedPath()
	found := false
	for _, handler := range HANDLERS {
		path_regex := handler.regex
		match := path_regex.FindStringSubmatch(path)
		if match != nil {
			path_elements := make(map[string]string)
			for i, name := range handler.regex.SubexpNames() {
				path_elements[name] = match[i]
			}
			handler.callback(site, path_elements, w, r)
			found = true
			break
		}
	}
	if !found {
		handle_not_found(site, w, r)
		return
	}
}

func SiteRenderer(
	settings base.SiteSettings,
	state *state.SiteState,
	search chan search.SearchQuery) http.HandlerFunc {
	templates, err_templates := load_templates(&settings)
	if err_templates != nil {
		log.Println(err_templates)
		panic("Unable to load templates!")
	}

	static, err_static := load_static_files(&settings)
	if err_static != nil {
		log.Println(err_static)
		panic("Unable to load static files!")
	}

	site := Site{
		Settings:  settings,
		State:     state,
		Templates: &templates,
		Static:    static,
		Search:    search,
	}

	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "text/html")
		route_request(site, w, r)
	}
}
