package main

import (
	"compress/gzip"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"

	"asma/api"
	"asma/base"
	"asma/search"
	"asma/server"
	"asma/site"
	"asma/state"
	"strconv"
)

var AsmArchiveVersion = "undefined"

func RenderTeapot(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusTeapot)
	w.Write([]byte("I'm a teapot\n"))
}

func RenderFaviconFunc(static_dir string) http.HandlerFunc {
	favicon_path := filepath.Join(static_dir, "favicon.ico")
	favicon_data, err := os.ReadFile(favicon_path)
	if err != nil {
		panic(fmt.Sprintf(
			"Unable to read favicon from %s: %v", favicon_path, err))
	}
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write(favicon_data)
	}
}

var gzPool = sync.Pool{
	New: func() interface{} {
		w, err := gzip.NewWriterLevel(io.Discard, gzip.BestSpeed)
		if err != nil {
			panic(fmt.Sprintf(
				"Unable to create gzip writer pool: %v", err))
		}
		return w
	},
}

type GzipResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

func (w *GzipResponseWriter) WriteHeader(status int) {
	w.Header().Del("Content-Length")
	w.ResponseWriter.WriteHeader(status)
}

func (w *GzipResponseWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}

func CompressGzipHandler(
	match_paths *regexp.Regexp,
	next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		encoding_type := ""
		encoding_header := r.Header.Get("Accept-Encoding")
		if strings.Contains(encoding_header, "x-gzip") {
			encoding_type = "x-gzip"
		} else if strings.Contains(encoding_header, "gzip") {
			encoding_type = "gzip"
		} else {
			next.ServeHTTP(w, r)
			return
		}
		if !match_paths.MatchString(r.URL.Path) {
			next.ServeHTTP(w, r)
			return
		}
		w.Header().Set("Content-Encoding", encoding_type)

		gz := gzPool.Get().(*gzip.Writer)
		defer gzPool.Put(gz)

		gz.Reset(w)
		defer gz.Close()
		gzip_writer := GzipResponseWriter{
			Writer:         gz,
			ResponseWriter: w,
		}
		next.ServeHTTP(&gzip_writer, r)
	})
}

func exit_forbidden(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusForbidden)
	w.Write([]byte("Can not exit without -dev mode!\n"))
}

func RenderLinks(settings base.SiteSettings) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(fmt.Sprintf(`<html>
<head><title>Namespaces</title></head>
<body>
<p>This offers following namespaces:</p>
<ul>
<li><a href="%s/">%s/</a> for database manipulation. Requires authentication.</li>
<li><a href="%s/">%s/</a> should be exposed through a reverse proxy as the site root</li>
<li><a href="/teapot/">/teapot/</a> I'm a teapot!</li>
<li><a href="/exit/">/exit/</a> make me quit, only in <code>-dev</code> mode</li>
</ul>
</body>
</html>
`,
			settings.ApiRoot,
			settings.ApiRoot,
			settings.SiteRoot,
			settings.SiteRoot)))
	}
}

// Terminate by client request.
func exit(w http.ResponseWriter, r *http.Request) {
	user, _, _ := r.BasicAuth()
	ip_address := r.RemoteAddr
	forwarded_for := r.Header.Get("X-Forwarded-For")

	log.Println(
		"Exit request from '" + user + "' at " + ip_address + " <" + forwarded_for + ">")

	os.Exit(0)
}

func add_env_string_flag(
	name string,
	default_value string,
	env_var string,
	help string) *string {
	env_read, ok := os.LookupEnv(env_var)
	if !ok {
		env_read = default_value
	}
	help_full := help
	help_full += " (environment variable: " + env_var + "=<value>)"
	return flag.String(name, env_read, help_full)
}

func main() {
	host := add_env_string_flag(
		"host", "localhost", "ASMA_HOST", "Virtualhost to listen to")
	root_site_f := add_env_string_flag(
		"root-site",
		"/site",
		"ASMA_ROOT_SITE",
		"Root path of the public site. Use domain name for virtualhost.")
	root_api_f := add_env_string_flag(
		"root-api",
		"/api",
		"ASMA_ROOT_API",
		"Root path of the API. Use domain name for virtualhost.")
	default_port_str, port_ok := os.LookupEnv("ASMA_PORT")
	if !port_ok {
		default_port_str = "8080"
	}
	default_port, port_str_err := strconv.Atoi(default_port_str)
	if port_str_err != nil {
		log.Fatalf("Invalid ASMA_PORT: %s", port_str_err)
	}
	port := flag.Int(
		"port",
		default_port,
		"Port to listen to (environment variable: ASMA_PORT=<value>)")
	data_dir := add_env_string_flag(
		"dir-data", "_data", "ASMA_DIR_DATA", "Data directory")
	static_dir := add_env_string_flag(
		"dir-static", "static", "ASMA_DIR_STATIC", "Static files directory")
	templates_dir := add_env_string_flag(
		"dir-templates", "templates", "ASMA_DIR_TEMPLATES", "Site templates directory")
	authfile := add_env_string_flag(
		"authfile", "auth.txt", "ASMA_AUTHFILE", "File with username:password lines")

	dev_str, dev_ok := os.LookupEnv("ASMA_DEV")
	dev_default := false
	if dev_ok {
		if dev_str == "1" {
			dev_default = true
		}
	}
	devmode := flag.Bool(
		"dev",
		dev_default,
		"Enable development mode (environment variable: ASMA_DEV=0/1)")

	flag.Parse()

	root_site := strings.TrimRight(*root_site_f, "/")
	root_api := strings.TrimRight(*root_api_f, "/")

	path_site_arr := strings.SplitN(root_site, "/", 2)
	path_site := ""
	if len(path_site_arr) == 2 {
		path_site = "/" + path_site_arr[1]
	}

	path_api_arr := strings.SplitN(root_api, "/", 2)
	path_api := ""
	if len(path_api_arr) == 2 {
		path_api = "/" + path_api_arr[1]
	}

	settings := base.SiteSettings{
		ApiRoot:      path_api,
		SiteRoot:     "",
		DataDir:      *data_dir,
		StaticDir:    *static_dir,
		TemplatesDir: *templates_dir,
		Version:      AsmArchiveVersion,
	}

	if *devmode {
		log.Println("Development mode enabled. DO NOT USE THIS IN PUBLIC! /exit is enabled!")
		settings.SiteRoot = path_site
		http.HandleFunc("/teapot/", RenderTeapot)
		http.HandleFunc("/exit/", exit)
	}
	http.HandleFunc("/", RenderLinks(settings))

	log.Printf("Recreating state from %s", settings.DataDir)
	state, err_state := state.New(settings.DataDir, settings.SiteRoot)
	if err_state != nil {
		log.Fatalf("Failed to create state: %s", err_state)
	}

	http.HandleFunc(
		fmt.Sprintf("%s/", root_api),
		server.StripPrefix(fmt.Sprintf("%s/", path_api),
			server.BasicAuth(*authfile, api.Renderer(settings, state))))

	search_channel := make(chan search.SearchQuery, 256)
	go search.SearchLoop(state, search_channel)
	http.Handle(
		fmt.Sprintf("%s/", root_site),
		CompressGzipHandler(
			regexp.MustCompile(""),
			server.StripPrefix(fmt.Sprintf("%s/", path_site),
				http.HandlerFunc(site.SiteRenderer(
					settings, state, search_channel)))))
	http.Handle(
		fmt.Sprintf("%s/_data/", root_site),
		server.AddCacheHeadersHandler(
			CompressGzipHandler(
				regexp.MustCompile("(ico|js|css|json)$"),
				http.StripPrefix(
					fmt.Sprintf("%s/_data/", path_site),
					http.FileServer(http.Dir(settings.DataDir))))))
	http.Handle(
		fmt.Sprintf("%s/_static/", root_site),
		server.AddCacheHeadersHandler(
			CompressGzipHandler(
				regexp.MustCompile("(ico|js|css|json)$"),
				http.StripPrefix(
					fmt.Sprintf("%s/_static/", path_site),
					http.FileServer(http.Dir(settings.StaticDir))))))
	http.Handle(
		fmt.Sprintf("%s/favicon.ico", root_site),
		server.AddCacheHeadersHandler(
			CompressGzipHandler(
				regexp.MustCompile(""),
				http.HandlerFunc(RenderFaviconFunc(settings.StaticDir)))))
	listen_addr := fmt.Sprintf("%s:%d", *host, *port)
	log.Printf("Listening to %s", listen_addr)
	log.Fatal(http.ListenAndServe(listen_addr, nil))
}
