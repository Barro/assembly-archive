package base

import (
	"crypto/sha256"
	"encoding/base64"
	"io"
	"os"
)

type SiteSettings struct {
	ApiRoot      string
	SiteRoot     string
	DataDir      string
	StaticDir    string
	TemplatesDir string
	Version      string
}

type Resolution struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type ImageInfo struct {
	Path     string
	FsPath   string
	Checksum string
	Size     Resolution
	Type     string
}

type Thumbnails struct {
	Default ImageInfo
	Sources []ImageInfo
}

type ExternalLink struct {
	Href     string
	Contents string
	Notes    string
}

type ExternalLinksSection struct {
	Name  string
	Links []ExternalLink
}

type Asset struct {
	Type string
	Data interface{}
}

// Structure that has all known data about an entry.
type Entry struct {
	Path          string
	Key           string
	Title         string
	Author        string
	Asset         Asset
	Description   string
	ExternalLinks []ExternalLinksSection
	Thumbnails    Thumbnails
}

func (entry *Entry) NotNil() *Entry {
	if entry == nil {
		return &Entry{}
	}
	return entry
}

type Section struct {
	Path        string
	Key         string
	Name        string
	Description string
	IsRanked    bool
	IsOngoing   bool
	Entries     []*Entry
}

func (section *Section) NotNil() *Section {
	if section == nil {
		return &Section{}
	}
	return section
}

type Year struct {
	Year     int
	Path     string
	Key      string
	Sections []*Section
}

func (year *Year) NotNil() *Year {
	if year == nil {
		return &Year{}
	}
	return year
}

// Creates a checksum of a file that is appropriate for caching.
func CreateFileChecksum(filename string) (string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer f.Close()

	hasher := sha256.New()
	if _, err := io.Copy(hasher, f); err != nil {
		return "", err
	}
	result_full := hasher.Sum(nil)
	str := base64.RawURLEncoding.EncodeToString(result_full[:6])
	// 48 bit wide checksum (6 bytes) nicely uses 8 base64 characters:
	return str[:8], nil
}
