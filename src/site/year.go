package site

import (
	"fmt"
	"log"
	"net/http"

	"asma/server"
)

type YearContext struct {
	Year      YearInfo
	Galleries []GalleryThumbnails
	Context   PageContext
}

func handle_year(
	site Site,
	path_elements map[string]string,
	w http.ResponseWriter,
	r *http.Request) {
	year, err_info := get_year_info(site, path_elements)
	if err_info != nil {
		redirect_url, err_redirect := get_redirect(site, path_elements)
		if err_redirect == nil {
			http.Redirect(w, r, *redirect_url, http.StatusMovedPermanently)
			return
		}
		log.Println(err_info)
		handle_not_found(site, w, r)
		return
	}

	page_context := PageContext{
		Path:  path_elements[""],
		Title: year.Curr.Key,
		Description: fmt.Sprintf(
			"Competitions and other events for %d Assembly parties",
			year.Curr.Year),
		Site:   site,
		Static: site.Static,
		Breadcrumbs: Breadcrumbs{
			Last: InternalLink{
				Path:     site.AddPrefix(year.Curr.Path),
				Contents: year.Curr.Key,
			},
		},
		YearlyNavigation: get_yearly_navigation(site, year.Curr.Year),
		CurrentYear:      year.Curr.Year,
		Navigation: PageNavigation{
			Prev: InternalLink{
				Path:     site.AddPrefix(year.Prev.NotNil().Path),
				Contents: year.Prev.NotNil().Key,
			},
			Next: InternalLink{
				Path:     site.AddPrefix(year.Next.NotNil().Path),
				Contents: year.Next.NotNil().Key,
			},
		},
	}

	page_cache_time := CACHE_TIME_DYNAMIC_PAGE_S
	prng := get_current_page_prng(page_cache_time)
	gallery_thumbnails := make([]GalleryThumbnails, len(year.Curr.Sections))
	for i, section := range year.Curr.Sections {
		var display_entries []DisplayEntry
		if section.IsRanked && !section.IsOngoing {
			preview_entries_count := MAX_PREVIEW_ENTRIES
			if len(section.Entries) < preview_entries_count {
				preview_entries_count = len(section.Entries)
			}
			preview_entries := section.Entries[:preview_entries_count]
			for _, entry := range preview_entries {
				display_entries = append(display_entries,
					DisplayEntry{
						Year:    year.Curr,
						Section: section,
						Entry:   entry,
					})
			}
		} else {
			display_entries = random_select_section_entries(
				prng, year.Curr, section, MAX_PREVIEW_ENTRIES)
		}
		thumbnails := GalleryThumbnails{
			Path:           section.Path,
			Title:          section.Name,
			GalleryEntries: display_entries,
		}
		gallery_thumbnails[i] = thumbnails
	}
	context := YearContext{
		Galleries: gallery_thumbnails,
		Year:      year,
		Context:   page_context,
	}
	add_cache_time(w, page_cache_time)
	err_template := render_template(w, site.Templates.Year, context)
	if err_template != nil {
		log.Printf("Internal year page error: %v", err_template)
		server.Ise(w)
	}
}
