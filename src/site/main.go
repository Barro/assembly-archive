package site

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"regexp"

	"asma/base"
	"asma/server"
)

const DEFAULT_MAIN_YEARS = 15
const MAX_MAIN_SECTION_ENTRIES = 2

var MAIN_DESCRIPTION = regexp.MustCompile(`\s+`).ReplaceAllString(
	`Assembly Archive offers a way to browse entries
	from all the years that Assembly has had parties on.`, " ")

type MainContext struct {
	Galleries   []GalleryThumbnails
	YearsBefore InternalLink
	YearsAfter  InternalLink
	Context     PageContext
}

// Randomly selects a number of entries by taking limited amount of
// entries from each section. There are basically many different
// possibilities to select viewable entries for the main page, but
// this is a simple unweighted logic that makes sure that one section
// with hundreds of entries does not dominate.
func random_select_entries(
	prng *rand.Rand, year *base.Year, amount int) []DisplayEntry {
	total_sections := len(year.Sections)
	section_indexes := prng.Perm(total_sections * MAX_MAIN_SECTION_ENTRIES)
	var result []DisplayEntry
	for _, index_value := range section_indexes {
		if len(result) == amount {
			break
		}
		index := index_value % total_sections
		section := year.Sections[index]
		entries := section.Entries
		if len(entries) == 0 {
			continue
		}
		entry := entries[prng.Intn(len(entries))]
		// Technically it's possible that we only get 1
		// entry/section. That's not a problem.
		if in_array(result, entry) {
			continue
		}
		result = append(result, DisplayEntry{
			Year:    year,
			Section: section,
			Entry:   entry,
		})
	}
	return result
}

func handle_main(
	site Site,
	path_elements map[string]string,
	w http.ResponseWriter,
	r *http.Request) {
	years, years_before, years_after := _read_year_range(site, r)
	page_cache_time := CACHE_TIME_DYNAMIC_PAGE_S
	prng := get_current_page_prng(page_cache_time)
	gallery_thumbnails := make([]GalleryThumbnails, len(years))
	for i, year := range years {
		gallery_thumbnails[i] = GalleryThumbnails{
			Path:  year.Path,
			Title: year.Key,
			GalleryEntries: random_select_entries(
				prng, year, MAX_PREVIEW_ENTRIES),
		}
	}
	breadcrumbs_last := ""
	var prefetches []Prefetch
	if len(years) > 0 {
		breadcrumbs_last = fmt.Sprintf(
			"%d-%d", years[len(years)-1].Year, years[0].Year)
		// Prefetch the latest year so that users can hopefully get it
		// faster.
		prefetches = []Prefetch{{
			Path: years[0].Path,
			Type: "document",
		}}
	}

	page_context := PageContext{
		Path:        path_elements[""],
		Description: MAIN_DESCRIPTION,
		Site:    site,
		Static:      site.Static,
		Navigation: PageNavigation{
			Prev: *years_before,
			Next: *years_after,
		},
		Breadcrumbs: Breadcrumbs{
			Last: InternalLink{
				Path:     "",
				Contents: breadcrumbs_last,
			},
		},
		YearlyNavigation: get_yearly_navigation(site, 0),
		Prefetches:       prefetches,
	}
	context := MainContext{
		Galleries:   gallery_thumbnails,
		YearsBefore: *years_before,
		YearsAfter:  *years_after,
		Context:     page_context,
	}
	add_cache_time(w, page_cache_time)
	err_template := render_template(w, site.Templates.Main, context)
	if err_template != nil {
		server.Ise(w)
		log.Printf("Internal main page error: %s", err_template)
	}
}
