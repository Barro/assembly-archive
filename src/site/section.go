package site

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"asma/base"
	"asma/server"
)

type SectionInfo struct {
	Year *base.Year
	Prev *base.Section
	Curr *base.Section
	Next *base.Section
}

type SectionContext struct {
	Year             *base.Year
	Section          SectionInfo
	DisplayEntries   []DisplayEntry
	OffsetNavigation PageNavigation
	Context          PageContext
}

func handle_section(
	site Site,
	path_elements map[string]string,
	w http.ResponseWriter,
	r *http.Request) {
	section, err_info := get_section_info(site, path_elements)
	if err_info != nil {
		redirect_url, err_redirect := get_redirect(site, path_elements)
		if err_redirect == nil {
			http.Redirect(w, r, *redirect_url, http.StatusMovedPermanently)
			return
		}
		log.Println(err_info)
		handle_not_found(site, w, r)
		return
	}

	offset_str := string(r.FormValue("offset"))
	offsets := create_offset_navigation(
		len(section.Curr.Entries), offset_str, MAX_SECTION_DISPLAY_ENTRIES)
	offset_navigation := PageNavigation{}
	if offsets.Prev != nil {
		offset_navigation.Prev = InternalLink{
			Contents: "Next " + strconv.Itoa(offsets.Prev.Items) + " items",
			Path:     "?offset=" + strconv.Itoa(offsets.Prev.Offset),
		}
	}
	if offsets.Next != nil {
		offset_navigation.Next = InternalLink{
			Contents: "Previous " + strconv.Itoa(offsets.Next.Items) + " items",
			Path:     "?offset=" + strconv.Itoa(offsets.Next.Offset),
		}
		if offsets.Next.Offset == 0 {
			offset_navigation.Next.Path = site.AddPrefix(path_elements[""])
		}
	}

	display_entries := make(
		[]DisplayEntry, offsets.ItemsEnd-offsets.ItemsStart)
	for i, entry := range section.Curr.Entries[offsets.ItemsStart:offsets.ItemsEnd] {
		display_entries[i] = DisplayEntry{
			Year:    section.Year,
			Section: section.Curr,
			Entry:   entry,
		}
	}

	title := section.Year.Key + " / " + section.Curr.Name
	page_context := PageContext{
		Path:  path_elements[""],
		Title: title,
		Description: fmt.Sprintf(
			"Entries for year %d section %s",
			section.Year.Year,
			section.Curr.Name),
		Site:   site,
		Static: site.Static,
		Breadcrumbs: Breadcrumbs{
			Parents: []InternalLink{
				{
					Path:     site.AddPrefix(section.Year.Path),
					Contents: section.Year.Key,
				},
			},
			Last: InternalLink{
				Path:     site.AddPrefix(section.Curr.Path),
				Contents: section.Curr.Name,
				Title:    section.Curr.Name,
			},
		},
		YearlyNavigation: get_yearly_navigation(site, section.Year.Year),
		CurrentYear:      section.Year.Year,
		Navigation: PageNavigation{
			Prev: InternalLink{
				Path:     site.AddPrefix(section.Prev.NotNil().Path),
				Contents: section.Prev.NotNil().Name,
				Title:    section.Prev.NotNil().Name,
			},
			Next: InternalLink{
				Path:     site.AddPrefix(section.Next.NotNil().Path),
				Contents: section.Next.NotNil().Name,
				Title:    section.Next.NotNil().Name,
			},
		},
	}
	context := SectionContext{
		DisplayEntries:   display_entries,
		OffsetNavigation: offset_navigation,
		Section:          section,
		Context:          page_context,
	}
	add_prefetch_links(&context.Context)
	add_cache_time(w, CACHE_TIME_STATIC_PAGE_S)
	err_template := render_template(w, site.Templates.Section, context)
	if err_template != nil {
		log.Printf("Internal main page error: %s", err_template)
		server.Ise(w)
	}
}
