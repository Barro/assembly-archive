package site

import (
	"errors"
	"fmt"
	"html"
	"log"
	"net/http"
	"strings"

	"asma/base"
	"asma/server"
	"asma/state"
)

func handle_asset_youtube(page_context PageContext, entry base.Entry) string {
	youtube := entry.Asset.Data.(state.YoutubeAsset)
	EMBED_TEMPLATE := `<iframe id="ytplayerembed" class="youtube-player" width="%d" height="%d" src="https://www.youtube.com/embed/%s" style="border: 0px" allowfullscreen="allowfullscreen">\n</iframe>`
	CONTROLS_HEIGHT := 0.0
	ASPECT_RATIO := 16.0 / 9.0
	DEFAULT_WIDTH := 640
	width := DEFAULT_WIDTH
	height := int(float64(width)/ASPECT_RATIO + CONTROLS_HEIGHT)
	embed_id := youtube.Id
	if strings.Contains(youtube.Id, "#t=") {
		splits := strings.SplitN(youtube.Id, "#t=", 2)
		id := splits[0]
		timestamp := splits[1]
		embed_id = fmt.Sprintf("%s?start=%s", id, timestamp)
	}
	return fmt.Sprintf(EMBED_TEMPLATE, width, height, html.EscapeString(embed_id))
}

func handle_asset_image(page_context PageContext, entry base.Entry) string {
	image := entry.Asset.Data.(state.ImageAsset)
	EMBED_TEMPLATE := `
<picture>
    %s
    <img src="%s" alt="%s" title="%s" width="%d" height="%d" />
</picture>
`
	image_path := fmt.Sprintf(
		"%s?%s",
		page_context.Site.AddPrefix(image.Default.Path),
		image.Default.Checksum)
	image_author_title := author_title(entry)
	return fmt.Sprintf(
		EMBED_TEMPLATE,
		view_image_srcset(page_context, image.Sources),
		html.EscapeString(image_path),
		html.EscapeString(image_author_title),
		html.EscapeString(image_author_title),
		image.Default.Size.X,
		image.Default.Size.Y,
	)
}

func get_entry_info(site Site, path_elements map[string]string) (EntryInfo, error) {
	info := EntryInfo{}
	section, info_err := get_section_info(site, path_elements)
	if info_err != nil {
		return info, info_err
	}
	info.Year = section.Year
	info.Section = section.Curr
	key := path_elements["Entry"]
	last_index := 0
	for i, candidate := range section.Curr.Entries {
		last_index = i
		if candidate.Key == key {
			info.Curr = candidate
			break
		}
		info.Next = candidate
	}
	if info.Curr == nil {
		return info, errors.New(
			fmt.Sprintf("Entry %s not found!", key))
	}
	if last_index+1 < len(section.Curr.Entries) {
		info.Prev = section.Curr.Entries[last_index+1]
	}
	return info, nil
}

type AssetHandler func(page_context PageContext, entry base.Entry) string

var ASSET_HANDLERS = map[string]AssetHandler{
	"youtube": handle_asset_youtube,
	"image":   handle_asset_image,
}

func handle_entry(
	site Site,
	path_elements map[string]string,
	w http.ResponseWriter,
	r *http.Request) {
	entry, err_info := get_entry_info(site, path_elements)
	if err_info != nil {
		redirect_url, err_redirect := get_redirect(site, path_elements)
		if err_redirect == nil {
			http.Redirect(w, r, *redirect_url, http.StatusMovedPermanently)
			return
		}
		log.Println(err_info)
		handle_not_found(site, w, r)
		return
	}

	page_context := PageContext{
		Path:   path_elements[""],
		Title:  author_title(*entry.Curr),
		Site:   site,
		Static: site.Static,
		Breadcrumbs: Breadcrumbs{
			Parents: []InternalLink{
				{
					Path:     site.AddPrefix(entry.Year.Path),
					Contents: entry.Year.Key,
				},
				{
					Path:     site.AddPrefix(entry.Section.Path),
					Contents: entry.Section.Name,
					Title:    entry.Section.Name,
				},
			},
		},
		YearlyNavigation: get_yearly_navigation(site, entry.Year.Year),
		CurrentYear:      entry.Year.Year,
		Navigation: PageNavigation{
			Prev: InternalLink{
				Path:     site.AddPrefix(entry.Prev.NotNil().Path),
				Contents: author_title(*entry.Prev.NotNil()),
				Title:    author_title(*entry.Prev.NotNil()),
			},
			Next: InternalLink{
				Path:     site.AddPrefix(entry.Next.NotNil().Path),
				Contents: author_title(*entry.Next.NotNil()),
				Title:    author_title(*entry.Next.NotNil()),
			},
		},
	}

	asset_handler, ok := ASSET_HANDLERS[entry.Curr.Asset.Type]
	if !ok {
		server.Ise(w)
		log.Printf(
			"No handler on %s for asset type %s",
			entry.Curr.Path,
			entry.Curr.Asset.Type)
		return
	}
	context := EntryContext{
		Year:    entry.Year,
		Section: entry.Section,
		Entry:   entry,
		Asset:   asset_handler(page_context, *entry.Curr),
		Context: page_context,
	}
	add_prefetch_links(&context.Context)
	add_cache_time(w, CACHE_TIME_STATIC_PAGE_S)
	err_template := render_template(w, site.Templates.Entry, context)
	if err_template != nil {
		log.Printf("Internal entry page error: %s", err_template)
		server.Ise(w)
	}
}
