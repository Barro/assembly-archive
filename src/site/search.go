package site

import (
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"asma/search"
	"asma/server"
)

const MAX_SEARCH_DISPLAY_ENTRIES = 50

type SearchContext struct {
	Query            string
	SearchResults    []*search.SearchResult
	TotalEntries     int
	Offset           int
	Limit            int
	DisplayEntries   DisplayEntries
	OffsetNavigation PageNavigation
	Context          PageContext
}

func (context SearchContext) MarshalJSON() ([]byte, error) {
	site_entries := make([]SiteEntry, len(context.DisplayEntries.Entries))
	for i, entry := range context.DisplayEntries.Entries {
		site_entries[i] = SiteEntry{
			Site:    context.Context.Site,
			Year:    entry.Year,
			Section: entry.Section,
			Entry:   entry.Entry,
		}
	}
	return json.Marshal(struct {
		TotalEntries int         `json:"total"`
		Offset       int         `json:"offset"`
		Limit        int         `json:"limit"`
		Entries      []SiteEntry `json:"entries"`
	}{
		TotalEntries: context.TotalEntries,
		Offset:       context.Offset,
		Limit:        context.Limit,
		Entries:      site_entries,
	})
}

func do_search(
	query_str string,
	search_channel chan search.SearchQuery) ([]*search.SearchResult, error) {
	var search_results []*search.SearchResult
	if strings.TrimSpace(query_str) == "" {
		return search_results, nil
	}
	query := search.SearchQuery{
		Query:  query_str,
		Result: make(chan []*search.SearchResult, 1),
	}
	select {
	case search_channel <- query:
	default:
		return search_results, fmt.Errorf("Search is blocking")
	}
	select {
	case search_results = <-query.Result:
	case <-time.After(5 * time.Second):
		return search_results, fmt.Errorf(
			"No search results in 5 seconds for %s", query.Query)
	}
	return search_results, nil
}

func handle_search(
	site Site,
	path_elements map[string]string,
	w http.ResponseWriter,
	r *http.Request) {
	query_str := string(r.FormValue("q"))

	page_context := PageContext{
		Path:             site.AddPrefix(r.URL.EscapedPath()),
		Title:            "Search",
		Description:      "Search",
		Site:             site,
		Query:            query_str,
		Static:           site.Static,
		YearlyNavigation: get_yearly_navigation(site, 0),
	}

	search_results, err_search := do_search(query_str, site.Search)
	context := SearchContext{
		Query:         query_str,
		Context:       page_context,
		SearchResults: search_results,
		TotalEntries:  len(search_results),
	}
	if err_search != nil {
		header := w.Header()
		header.Add("Retry-After", fmt.Sprintf("%d", 10))
		w.WriteHeader(http.StatusTooManyRequests)
		err_template := render_template(w, site.Templates.Overload, context)
		if err_template != nil {
			log.Printf(
				"Failed to render http.StatusTooManyRequests: %s", err_template)
		}
		log.Println(err_search)
		return
	}

	display_entries := DisplayEntries{
		Context:             &page_context,
		ShowThumbnailHeader: true,
	}
	offset_str := string(r.FormValue("offset"))

	limit_str := string(r.FormValue("limit"))
	limit, err_limit := strconv.Atoi(limit_str)
	if err_limit != nil {
		limit = MAX_SEARCH_DISPLAY_ENTRIES
	}
	if MAX_SEARCH_DISPLAY_ENTRIES < limit {
		limit = MAX_SEARCH_DISPLAY_ENTRIES
	}
	if limit < 1 {
		limit = MAX_SEARCH_DISPLAY_ENTRIES
	}
	context.Limit = limit
	limit_urlpart := ""
	if limit != MAX_SEARCH_DISPLAY_ENTRIES {
		limit_urlpart = fmt.Sprintf("&amp;limit=%d", limit)
	}

	offsets := create_offset_navigation(
		len(context.SearchResults), offset_str, limit)
	context.Offset = offsets.CurrentOffset

	result_entries := make(
		[]DisplayEntry, offsets.ItemsEnd-offsets.ItemsStart)
	for i, result := range context.SearchResults[offsets.ItemsStart:offsets.ItemsEnd] {
		result_entries[i] = DisplayEntry{
			Year:    result.Year,
			Section: result.Section,
			Entry:   result.Entry,
		}
	}
	display_entries.Entries = result_entries
	context.DisplayEntries = display_entries
	offset_navigation := PageNavigation{}
	query_html := html.EscapeString(query_str)
	if offsets.Prev != nil {
		offset_navigation.Prev = InternalLink{
			Contents: fmt.Sprintf("Next %d results", offsets.Prev.Items),
			Path: fmt.Sprintf(
				"?q=%s&amp;offset=%d%s",
				query_html, offsets.Prev.Offset, limit_urlpart),
		}
	}
	if offsets.Next != nil {
		offset_navigation.Next = InternalLink{
			Contents: fmt.Sprintf("Previous %d results", offsets.Next.Items),
			Path: fmt.Sprintf(
				"?q=%s&amp;offset=%d%s",
				query_html, offsets.Next.Offset, limit_urlpart),
		}
		if offsets.Next.Offset == 0 {
			offset_navigation.Next.Path = fmt.Sprintf(
				"?q=%s%s", query_html, limit_urlpart)
		}
	}
	context.OffsetNavigation = offset_navigation

	page_format := get_page_format(r)

	if page_format == FMT_JSON {
		out_json, err_json := json.Marshal(context)
		if err_json != nil {
			server.Ise(w)
			log.Printf("Failed to marshal JSON: %s", err_json)
			return
		}
		add_cache_time(w, CACHE_TIME_STATIC_PAGE_S)
		render_json(w, out_json)
		return
	}
	if page_format != FMT_HTML {
		server.Ise(w)
		log.Printf("Got unexpected page format: %d", page_format)
		return
	}
	add_cache_time(w, CACHE_TIME_STATIC_PAGE_S)
	err_template := render_template(w, site.Templates.Search, context)
	if err_template != nil {
		log.Print("Failed to render search template: %s", err_template)
		server.Ise(w)
	}
}
