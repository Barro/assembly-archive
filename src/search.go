package search

import (
	"fmt"
	"log"
	"regexp"
	"sort"
	"strings"
	"unicode"
	"unsafe"

	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"

	"asma/base"
	"asma/state"
)

// Arbitrary limits for queries to limit abuse:
const MAX_QUERY_LENGTH = 63
const MAX_QUERY_KEYWORDS = 8

// Have a relatively small query cache. If we're denial of serviced,
// then rather exhaust CPU instead of the memory.
const MAX_CACHE_MULTIPLIER = 16
const CACHE_FREE_RATIO float64 = 0.7

type SearchResult struct {
	Year    *base.Year
	Section *base.Section
	Entry   *base.Entry
}

type SearchQuery struct {
	Query  string
	Result chan []*SearchResult
}

func NormalizeTerms(terms_raw string) string {
	terms_limited := terms_raw
	if len(terms_limited) > MAX_QUERY_LENGTH {
		terms_limited = terms_limited[:MAX_QUERY_LENGTH]
	}

	// This should have good enough normalization properties for the
	// languages that Assembly entries are in.
	terms_lower := strings.ToLower(terms_limited)
	t := transform.Chain(
		norm.NFKD,
		runes.Remove(runes.In(unicode.Me)),
		runes.Remove(runes.In(unicode.Mn)),
		runes.Remove(runes.In(unicode.Other)),
	)
	terms_filtered, _, _ := transform.String(t, terms_lower)

	return terms_filtered
}

func SplitKeywords(terms string) []string {
	words := []string{}
	unquoted := terms
	for len(unquoted) > 0 {
		b_q_a := strings.SplitN(unquoted, "\"", 3)
		if len(b_q_a) == 3 {
			before := b_q_a[0]
			between := b_q_a[1]
			after := b_q_a[2]
			words = append(words, between)
			unquoted = after + " " + before
			continue
		}
		words_part := strings.Split(unquoted, " ")
		for _, word := range words_part {
			if len(word) == 0 {
				continue
			}
			words = append(words, word)
		}
		break
	}

	unique := []string{}
	sort.Strings(words)
	prev_word := ""
	for _, word := range words {
		if word == prev_word {
			continue
		}
		prev_word = word
		unique = append(unique, word)
	}
	words = unique

	sort.Slice(
		words,
		func(i int, j int) bool { return len(words[i]) > len(words[j]) })
	if len(words) > MAX_QUERY_KEYWORDS {
		words = words[:MAX_QUERY_KEYWORDS]
	}
	return words
}

type SearchYearSection struct {
	Normalized string
	Year       *base.Year
	Section    *base.Section
}

type SearchEntry struct {
	Normalized  string
	YearSection *SearchYearSection
	Entry       *base.Entry
}

func get_search_entries(site_state *state.SiteState) []SearchEntry {
	var search_cache []SearchEntry
	nohtml_regexp := regexp.MustCompile("<[^>]+>")
	for _, year := range site_state.Years {
		for _, section := range year.Sections {
			prefix := fmt.Sprintf(
				"Y:%d|S:%s|%d %s",
				year.Year,
				NormalizeTerms(section.Name),
				year.Year,
				NormalizeTerms(section.Name))
			year_section := SearchYearSection{
				Normalized: prefix,
				Year:       year,
				Section:    section,
			}
			for _, entry := range section.Entries {
				description_nohtml := nohtml_regexp.ReplaceAllString(
					entry.Description, "")
				entry_search := fmt.Sprintf(
					"A:%s|T:%s|%s by %s|%s",
					NormalizeTerms(entry.Author),
					NormalizeTerms(entry.Title),
					NormalizeTerms(entry.Title),
					NormalizeTerms(entry.Author),
					NormalizeTerms(description_nohtml))
				search_entry := SearchEntry{
					Normalized:  entry_search,
					YearSection: &year_section,
					Entry:       entry,
				}
				search_cache = append(search_cache, search_entry)
			}
		}
	}
	return search_cache
}

type CachedResult struct {
	LastAccess int64
	Result     []*SearchResult
}

type SearchCache struct {
	// All data is managed per section basis. This caches the known
	// section pointers and cache entries are re-created when the state
	// of sections changes on updated data. This has great potential
	// for race conditions...
	Sections      []*base.Section
	Entries       []SearchEntry
	MaxSize       int64
	UsedSize      int64
	NextTimestamp int64
	QueryResults  map[string]*CachedResult
}

func get_state_sections(site_state *state.SiteState) []*base.Section {
	state_sections := []*base.Section{}
	for _, year := range site_state.Years {
		state_sections = append(state_sections, year.Sections...)
	}
	return state_sections
}
func cache_is_valid(
	state_sections []*base.Section, cache *SearchCache) bool {
	if len(state_sections) != len(cache.Sections) {
		return false
	}
	for i := 0; i < len(state_sections); i++ {
		if state_sections[i] != cache.Sections[i] {
			return false
		}
	}
	return true
}

func manage_query_cache_space(cache *SearchCache) *SearchCache {
	if cache.UsedSize <= cache.MaxSize {
		return cache
	}
	log.Printf(
		"Query cache size %d exceeded its maximum size %d at %d. Removing oldest entries.",
		cache.UsedSize,
		cache.MaxSize,
		cache.NextTimestamp)
	target_cache_size := int64(
		CACHE_FREE_RATIO * float64(cache.MaxSize))
	data_removal_size := cache.UsedSize - target_cache_size
	type SearchMapEntry struct {
		Key    string
		Cached *CachedResult
	}
	map_items := make([]SearchMapEntry, len(cache.QueryResults))
	i := 0
	for key, value := range cache.QueryResults {
		map_items[i] = SearchMapEntry{
			Key:    key,
			Cached: value,
		}
		i++
	}
	sort.Slice(map_items, func(i, j int) bool {
		return map_items[i].Cached.LastAccess < map_items[j].Cached.LastAccess
	})
	total_removed := int64(0)
	for _, entry := range map_items {
		if total_removed >= data_removal_size {
			break
		}
		delete(cache.QueryResults, entry.Key)
		total_removed += query_result_size(entry.Cached.Result, entry.Key)
	}
	cache.UsedSize -= total_removed
	return cache
}

func get_search_cache(
	site_state *state.SiteState, cache *SearchCache) *SearchCache {
	state_sections := get_state_sections(site_state)
	if cache_is_valid(state_sections, cache) {
		return cache
	}

	log.Print("Recreating search cache")
	// There can be a race condition between the check and this
	// function call. No worries, the cache will in updated after the
	// next query. Also the potential out of datedness of the cache
	// only covers a very small portion of the results.
	cache_entries := get_search_entries(site_state)
	cache.Sections = state_sections
	cache.Entries = cache_entries

	max_cache_size := MAX_CACHE_MULTIPLIER * query_result_size(
		make([]*SearchResult, len(cache.Entries)),
		strings.Repeat("a", MAX_QUERY_LENGTH))
	log.Printf(
		"Have %0.1f MiB query cache for %d entries",
		float64(max_cache_size)/1024.0/1024.0,
		len(cache.Entries))
	cache.MaxSize = max_cache_size
	cache.UsedSize = 0
	cache.NextTimestamp = 1
	cache.QueryResults = map[string]*CachedResult{}
	return cache
}

func ConvertSearchOperators(keywords []string) []string {
	result := []string{}
	for _, keyword := range keywords {
		if len(keyword) < 3 {
			result = append(result, keyword)
			continue
		}
		converted := keyword
		for _, operator := range []string{"a:", "s:", "t:", "y:"} {
			if !strings.HasPrefix(keyword, operator) {
				continue
			}
			converted = strings.ToUpper(operator) + keyword[2:] + "|"
			break
		}
		result = append(result, converted)
	}
	return result
}

/**
 * Return the approximate size of a query and results for cache
 * management purposes.
 */
func query_result_size(result []*SearchResult, query_normalized string) int64 {
	query_size := unsafe.Sizeof(query_normalized)
	array_size := unsafe.Sizeof(result)
	cache_entry_size := unsafe.Sizeof(CachedResult{})
	result_size := unsafe.Sizeof(SearchResult{})
	return int64(query_size +
		uintptr(len(query_normalized)) +
		cache_entry_size +
		array_size +
		result_size*uintptr(len(result)))
}

func search_from_cache(cache *SearchCache, query string) []*SearchResult {
	query_normalized := NormalizeTerms(query)
	keywords := SplitKeywords(query_normalized)
	keywords = ConvertSearchOperators(keywords)
	if len(keywords) == 0 {
		return []*SearchResult{}
	}

	if cached, ok := cache.QueryResults[query_normalized]; ok {
		cached.LastAccess = cache.NextTimestamp
		cache.NextTimestamp++
		return cached.Result
	}

	result := []*SearchResult{}
	for _, entry := range cache.Entries {
		failed := false
		for _, keyword := range keywords {
			if strings.Contains(entry.Normalized, keyword) {
				continue
			}
			if strings.Contains(entry.YearSection.Normalized, keyword) {
				continue
			}
			failed = true
			break
		}
		if failed {
			continue
		}
		result = append(result, &SearchResult{
			Year:    entry.YearSection.Year,
			Section: entry.YearSection.Section,
			Entry:   entry.Entry,
		})
	}

	query_cache_result := &CachedResult{
		LastAccess: cache.NextTimestamp,
		Result:     result,
	}
	cache.QueryResults[query_normalized] = query_cache_result
	cache.UsedSize += query_result_size(result, query_normalized)
	cache.NextTimestamp++
	manage_query_cache_space(cache)
	return result
}

func InitSearch(site_state *state.SiteState) *SearchCache {
	cache := &SearchCache{}
	cache = get_search_cache(site_state, cache)
	return cache
}

func DoSearch(
	site_state *state.SiteState,
	search_channel chan SearchQuery,
	cache *SearchCache) {
	search := <-search_channel
	cache = get_search_cache(site_state, cache)
	result := search_from_cache(cache, search.Query)
	search.Result <- result
	close(search.Result)
}

func SearchLoop(
	site_state *state.SiteState,
	search_channel chan SearchQuery) {
	cache := InitSearch(site_state)
	for {
		DoSearch(site_state, search_channel, cache)
	}
}
